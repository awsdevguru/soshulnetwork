/*
 * SoshulNetwork
 *
 * Description: Get information about the currently authenticated user
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

var user_pool_id = "INSERT_USER_POOL_ID";

async function getUser(sub){

  var ret = {"error":"", "results":{}};
  
  var params = {
    Username: sub,
    UserPoolId: user_pool_id
  }
  
  try {
    await cognitoidentityserviceprovider.adminGetUser(params, function(err, data){
      if (err) {
        console.log(err);
        ret.error = err.message;
      } else {
        console.log('user_info: ' + JSON.stringify(data));
        for (var i = 0; i < data.UserAttributes.length; i++) {
          var item = data.UserAttributes[i];
          ret.results[item.Name] = item.Value;
        }
      };
    }).promise();
    return ret;
  } catch (err) {
    ret.error = err.message;
    return ret;
  }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
    console.log(JSON.stringify(event));
  
    var sub = event.requestContext.authorizer.claims.sub;
  
    console.log("get_user:: " + sub);
    
    try {
        ret = await getUser(sub);
    } catch (err) {
        console.log("error: " + err);
        ret.error = err;
    }
    
    
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
