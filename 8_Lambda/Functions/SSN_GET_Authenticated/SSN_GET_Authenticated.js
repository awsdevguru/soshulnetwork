/*
 * SoshulNetwork
 *
 * Description: Check if user is currently authenticated via Cognito
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

exports.handler = async (event, context) => {
    var ret = {"error":"", "authenticated":false};
    
    if ("sub" in event.requestContext.authorizer.claims) {
        ret.authenticated = true;
    } else {
        ret.error = "Not authenticated";
    }

    const response = {
        statusCode: 200,
		headers: { 
  		    'Content-Type': 'application/json',
      		'Access-Control-Allow-Origin': 'https://live.soshul.network'
		},
        body: JSON.stringify(ret)
    };
    return response;
};
