/*
 * SoshulNetwork
 *
 * Description: Return error response that this feature is not implemented yet.
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 * NOTE:
 * This function does not account for the max size of returned data (1MB) and the 
 * resulting pagination of results.  That exercise is left to the reader.
 */

const AWS = require('aws-sdk');

exports.handler = async (event, context) => {
    var ret = {"error":"Not implemented yet.", "results":{}};
    
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};

