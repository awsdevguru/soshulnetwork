#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_ANY_Not_Implemented.zip SSN_ANY_Not_Implemented.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_ANY_Not_Implemented" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_ANY_Not_Implemented.zip" --runtime "nodejs14.x" --handler "SSN_ANY_Not_Implemented.handler" --tracing-config '{"Mode": "Active"}'

