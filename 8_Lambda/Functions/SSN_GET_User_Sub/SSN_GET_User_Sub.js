/*
 * SoshulNetwork
 *
 * Description: Get user information by sub 
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

var user_pool_id = "INSERT_USER_POOL_ID";

async function getUser(sub){

  var ret = {"error":"", "results":{}};
  
  var params = {
    Username: sub,
    UserPoolId: user_pool_id
  }
  
  try {
    await cognitoidentityserviceprovider.adminGetUser(params, function(err, data){
      if (err) {
        console.log(err);
        ret.error = err.message;
      } else {
        console.log(data)
        // ret.results = data;
        for (var i = 0; i < data.UserAttributes.length; i++) {
          var item = data.UserAttributes[i];
          //Don't leak user's email address info.
          if (!(item.Name === "email") && !(item.Name === "email_verified")) {
            ret.results[item.Name] = item.Value;
          }
        }
      };
    }).promise();
    return ret;
  } catch (err) {
    ret.error = err.message;
    return ret;
  }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
  
    var sub = event.pathParameters.sub;
  
    console.log("get_user:: " + sub);
    
    try {
        ret = await getUser(sub);
    } catch (err) {
        ret.error = err;
    }
    
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
