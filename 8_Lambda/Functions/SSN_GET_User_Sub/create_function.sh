#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_GET_User_Sub.zip SSN_GET_User_Sub.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_GET_User_Sub" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_GET_User_Sub.zip" --runtime "nodejs14.x" --handler "SSN_GET_User_Sub.handler" --tracing-config '{"Mode": "Active"}'

