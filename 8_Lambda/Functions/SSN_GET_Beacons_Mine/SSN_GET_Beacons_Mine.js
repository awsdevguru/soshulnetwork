/*
 * SoshulNetwork
 *
 * Description: Get a list of beacons created by the logged in user making the call.
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

async function queryBeaconsForSub(sub_uuid) {
  console.log("queryBeaconsForSub():: " + sub_uuid);
  
  var beacon_query_params = {
    TableName: 'SoshulNetworkLive',
    IndexName: 'sub_uuid-index',
    KeyConditionExpression: '#pk = :pk_value and #su = :su_value',
    ExpressionAttributeValues: { ':pk_value': 'beacons', ':su_value': sub_uuid},
    ExpressionAttributeNames: { '#pk': 'pk', '#su': 'sub_uuid' }
  }


  try {
    const data = await docClient.query(beacon_query_params).promise()
    console.log("returning data: " + JSON.stringify(data));
    return data
  } catch (err) {
    console.log("returning error: " + JSON.stringify(err));
    return err
  }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
    
    try {
        ret.results = await queryBeaconsForSub(event.requestContext.authorizer.claims.sub)
        console.log("ret is now: " + JSON.stringify(ret));
    } catch (err) {
        ret.error = err;
        console.log("ret is now: " + JSON.stringify(ret));
    }
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
