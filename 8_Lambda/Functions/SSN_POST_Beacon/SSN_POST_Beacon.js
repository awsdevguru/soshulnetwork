/*
 * SoshulNetwork
 *
 * Description: Create a beacon
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

var AWSXRay = require('aws-xray-sdk');
const AWS = AWSXRay.captureAWS(require('aws-sdk'))
const docClient = new AWS.DynamoDB.DocumentClient();

const sqs_queue_url = "INSERT_SQS_QUEUE_URL";
const sqs_queue_region = "INSERT_SQS_QUEUE_REGION";

var sqs = new AWS.SQS({"region": sqs_queue_region });

async function postBeacon(uuid, beacon_data, beacon_type, beacon_color, beacon_title, sub_uuid){
  var now = Date.now();
  const params = {
    TableName : 'SoshulNetworkLive',
    Item: {
       pk: 'beacons',
       sk: uuid,
       beacon_data: beacon_data,
       beacon_type: beacon_type,
       beacon_title: beacon_title,
       beacon_color: beacon_color,
       sub_uuid: sub_uuid,
       published_at: now
    }
  }
  
  try {
    publish_beacon_to_sqs_queue(params);
    await docClient.put(params).promise();
  } catch (err) {
    return err;
  }
}

async function publish_beacon_to_sqs_queue(beacon) {
    var sqs_params = {
      MessageBody: JSON.stringify(beacon), 
      QueueUrl: sqs_queue_url
    };
    sqs.sendMessage(sqs_params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
    });
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "result":""};
    console.log(event);
    
    try {
        var body = JSON.parse(event.body);
        
        console.log(body);
    
        if ("beacon_data" in body && "beacon_type" in body && "beacon_title" in body) {
            if (!("beacon_color" in body)) {
                body.beacon_color = "primary";
            }
            const result = await postBeacon(context.awsRequestId, body.beacon_data, body.beacon_type, body.beacon_color, body.beacon_title, event.requestContext.authorizer.claims.sub);
            ret.result = "Beacon Posted";
        } else {
            ret.error = "Malformed Beacon POST";
        }
    } catch (err) {
        ret.error = err;
        console.log(err);
    }
    
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
