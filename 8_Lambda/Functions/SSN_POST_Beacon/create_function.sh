#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Installing Node Packages
npm install

echo Packaging
rm *.zip
zip -r SSN_POST_Beacon.zip SSN_POST_Beacon.js node_modules/

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_POST_Beacon" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_POST_Beacon.zip" --runtime "nodejs14.x" --handler "SSN_POST_Beacon.handler" --tracing-config '{"Mode": "Active"}'

