#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_Beacon_from_SQS_to_followers.zip SSN_Beacon_from_SQS_to_followers.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_Beacon_from_SQS_to_followers" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_Beacon_from_SQS_to_followers.zip" --runtime "nodejs14.x" --handler "SSN_Beacon_from_SQS_to_followers.handler" --tracing-config '{"Mode": "Active"}'

