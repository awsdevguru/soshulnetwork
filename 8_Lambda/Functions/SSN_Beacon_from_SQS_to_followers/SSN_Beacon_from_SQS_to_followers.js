/*
 * SoshulNetwork
 *
 * Description: Call SES to email followers of a beacon's publisher.
 *              Triggered by SQS content ingestion, from SQS_POST_Beacon
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
var ses = new AWS.SES({region: 'us-west-1'});

var user_pool_id = "INSERT_USER_POOL_ID"

exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
    console.log(JSON.stringify(event));
    
    /*
     * For each record sent from SQS
     */
    for (var i = 0; i < event.Records.length; i++) {
        var record = event.Records[i];
        console.log("handler():: item: " + JSON.stringify(record));
        
        /*
         * Query DDB for the followers object related to the content creator
         */
        try {
            var beacon = JSON.parse(record.body);
            console.log("handler():: parsed beacon data: " + JSON.stringify(beacon));
            
            //get followers from DDB
            //for each follower, get email address
            //publish to SES
            
            //get followers
            const params = {
                TableName : 'SoshulNetworkLive',
                Key: {
                  pk: 'followers',
                  sk: beacon.Item.sub_uuid
                }
              }
            
            try {
                const followers_data = await docClient.get(params).promise();
                console.log("handler():: the data: " + JSON.stringify(followers_data));
                
                var followers = []; 
                if ("Item" in followers_data) {
                  followers = followers_data.Item.followers;
                } 
                console.log("handler():: followers array is: " + JSON.stringify(followers));
                
                /*
                 * Get *name* of content poster
                 */
                var poster_user_data_params = {
                    Username: beacon.Item.sub_uuid,
                    UserPoolId: user_pool_id
                };
                const poster_user_data = await cognitoidentityserviceprovider.adminGetUser(poster_user_data_params).promise();
                console.log("handler():: poster_user_data: " + JSON.stringify(poster_user_data));

                var poster_name = "";
                for (var o = 0; o < poster_user_data.UserAttributes.length; o++) {
                    if (poster_user_data.UserAttributes[o].Name === "name") {
                        poster_name = poster_user_data.UserAttributes[o].Value;
                    }
                }
                console.log("handler():: Extracted name of poster as: " + poster_name);
                
                /*
                 * For each follower of the content creator, get email of follower
                 */
                for (var i = 0; i < followers.length; i++) {
                    var follower = followers[i];
                    console.log("handler():: follower: " + follower);
                    
                    /*
                     * get email address of *this* follower
                     */
                    var follower_user_data_params = {
                        Username: follower,
                        UserPoolId: user_pool_id
                    };
                    const follower_user_data = await cognitoidentityserviceprovider.adminGetUser(follower_user_data_params).promise();
                    console.log("handler():: user_data: " + JSON.stringify(follower_user_data));
                    
                    var sub_email = "";
                    var sub_name = "";
                    for (var o = 0; o < follower_user_data.UserAttributes.length; o++) {
                        if (follower_user_data.UserAttributes[o].Name === "email") {
                            sub_email = follower_user_data.UserAttributes[o].Value;
                        } else if (follower_user_data.UserAttributes[o].Name === "name") {
                            sub_name = follower_user_data.UserAttributes[o].Value;
                        }
                    }
                    console.log("handler():: Extracted email address for this follower as: " + sub_email);
                    
                    
                     
                    /*
                     * Send email with SES
                     */
                     var ses_params = {
                          Destination: { 
                            ToAddresses: [
                              sub_email
                            ]
                          },
                          Message: { 
                            Body: { 
                              Html: {
                                Data: "New beacon from "+poster_name+". View it <a href='https://live.soshul.network/beacon.html#uuid="+beacon.Item.sk+"'>here</a>"
                              }
                            },
                            Subject: { 
                              Data: "New Beacon!"
                            }
                          },
                          Source: 'no-reply@live.soshul.network'
                    };
                    // await ses.sendEmail(ses_params, function(err, data) {
                    //   if (err) console.log(err, err.stack); // an error occurred
                    //   else     console.log(data);           // successful response
                    // });
                    const email_result = await ses.sendEmail(ses_params).promise();
                    console.log("handler():: email_result: " + JSON.stringify(email_result));
                    
                }
            } catch (err) {
                console.log("handler():: caught error: " + err);
            }
            
        } catch (err) {
            console.log("handler():: caught error: " + err);
        }
    }
};

