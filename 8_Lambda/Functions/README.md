# Lambda Functions
One directory per function.  Directory contains the actual source code.  The shell script uses AWS CLI to create the function and upload the code.  IAM Policy and Lambda Execution Role should have been created.

### Example AWS CLI Script
```
$ cat create_function.sh
#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_GET_Beacons.zip SSN_GET_Beacons.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_GET_Beacons" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_GET_Beacons.zip" --runtime "nodejs14.x" --handler "SSN_GET_Beacons.handler" --tracing-config '{"Mode": "Active"}'
```

### Example Output
```
$ ./create_function.sh
Packaging
  adding: SSN_GET_Beacons.js (deflated 63%)
Create Function
{
    "FunctionName": "SSN_GET_Beacons",
    "FunctionArn": "arn:aws:lambda:us-west-1:146868985163:function:SSN_GET_Beacons",
    "Runtime": "nodejs14.x",
    "Role": "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live",
    "Handler": "SSN_GET_Beacons.handler",
    "CodeSize": 1798,
    "Description": "",
    "Timeout": 3,
    "MemorySize": 128,
    "LastModified": "2022-01-19T02:03:42.731+0000",
    "CodeSha256": "c05E5gftHlWl4jkiBIL8lglwWn2AVTLB+6+861dpH8I=",
    "Version": "$LATEST",
    "TracingConfig": {
        "Mode": "Active"
    },
    "RevisionId": "8f261f8b-fa92-426c-aa91-5e01f93e1b87",
    "State": "Pending",
    "StateReason": "The function is being created.",
    "StateReasonCode": "Creating",
    "PackageType": "Zip"
}
```
