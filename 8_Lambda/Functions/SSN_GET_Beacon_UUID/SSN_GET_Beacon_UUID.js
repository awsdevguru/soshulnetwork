/*
 * SoshulNetwork
 *
 * Description: Get a particular beacon by UUID
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

var user_info_cache = [];

const one_day_in_seconds = 86400;

async function getUserInfoForSub(sub) {
  console.log("getUserInfoForSub():: " + sub);
  var ret = {"sub":"", "name":""};
  
  //check local cache prior to querying DDB
  var now = Math.round(Date.now() / 1000); // for age check.
  for (var i = 0; i < user_info_cache.length; i++) {
    if (user_info_cache[i].sub === sub) {
      if (now - user_info_cache[i].cache_time < one_day_in_seconds) {
        ret.sub = user_info_cache[i].sub;
        ret.name = user_info_cache[i].name;
        console.log("getUserInfoForSub():: found a match in cache: " +  (now - user_info_cache[i].cache_time) + " seconds old.");

        return ret;
      } else {
        //we had a match but it's too old, get out of this loop.
        console.log("getUserInfoForSub():: found a match in cache but it's too old: " +  (now - user_info_cache[i].cache_time));
        break;
      }
    }
  }
  
  var user_pool_id = "INSERT_USER_POOL_ID";
  
  var params = {
    Username: sub,
    UserPoolId: user_pool_id
  }
  
  try {
    await cognitoidentityserviceprovider.adminGetUser(params, function(err, data){
      if (err) {
        console.log(err);
      } else {
        console.log("getUserInfoForSub():: RX data from Cognito for user: " + JSON.stringify(data));
        // ret.results = data;
        for (var i = 0; i < data.UserAttributes.length; i++) {
          var item = data.UserAttributes[i];
          //Don't leak user's email address info.
          if (!(item.Name === "email") && !(item.Name === "email_verified")) {
            ret[item.Name] = item.Value;
          }
        }
      };
    }).promise();
    
    //store to local cache for future use.
    ret.cache_time = Math.round(Date.now() / 1000)
    user_info_cache.push(ret);
    console.log("getUserInfoForSub():: added user info to the cache. cache is now: " + JSON.stringify(user_info_cache));
    
    return ret;
  } catch (err) {
    return ret;
  }
  
}


async function getBeacon(uuid){
  const params = {
    TableName : 'SoshulNetworkLive',
    Key: {
      pk: 'beacons',
      sk: uuid
    }
  }
  
  try {
    const data = await docClient.get(params).promise();
    console.log("the data: " + JSON.stringify(data));
    if ("Item" in data) {
      var user_info = await getUserInfoForSub(data.Item.sub_uuid);
      data.Item.user_info = user_info;
    }
    return data
  } catch (err) {
    return err
  }
}

exports.handler = async (event, context) => {
  
    console.log("get_beacon:: " + event.pathParameters.uuid);
    console.log(event);
    
    var ret = {"error":"", "results":{}};
    
    try {
        ret.results = await getBeacon(event.pathParameters.uuid)
        console.log("ret is now: " + JSON.stringify(ret));
    } catch (err) {
        ret.error = err;
        console.log("ret is now: " + JSON.stringify(ret));
    }
    
    if (!("Item" in ret.results)) {
      ret.error = "Beacon not found. UUID: " + event.pathParameters.uuid;
    }
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
