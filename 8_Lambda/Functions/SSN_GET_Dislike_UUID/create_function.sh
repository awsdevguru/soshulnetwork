#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_GET_Dislike_UUID.zip SSN_GET_Dislike_UUID.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_GET_Dislike_UUID" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_GET_Dislike_UUID.zip" --runtime "nodejs14.x" --handler "SSN_GET_Dislike_UUID.handler" --tracing-config '{"Mode": "Active"}'

