/*
 * SoshulNetwork
 *
 * Description: Get a count of dislikes for a particular beacon
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 * NOTE:
 * This function does not account for the max size of returned data (1MB) and the 
 * resulting pagination of results.  That exercise is left to the reader.
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

async function queryDislikesForBeacon(beacon_uuid) {
  console.log("queryDislikesForBeacon():: " + beacon_uuid);
  
  var dislikes_query_params = {
    TableName: 'SoshulNetworkLive',
    IndexName: 'beacon_uuid-index',
    KeyConditionExpression: '#pk = :pk_value and #bu = :bu_value',
    ExpressionAttributeValues: { ':pk_value': 'dislikes', ':bu_value': beacon_uuid},
    ExpressionAttributeNames: { '#pk': 'pk', '#bu': 'beacon_uuid' }
  }


  try {
    const data = await docClient.query(dislikes_query_params).promise()
    console.log("returning data: " + JSON.stringify(data));
    return data
  } catch (err) {
    console.log("returning error: " + JSON.stringify(err));
    return err
  }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
    console.log(event);
    
    if (!("uuid" in event.pathParameters)) {
      ret.error("Missing UUID");
    } else {
      try {
          var results = await queryDislikesForBeacon(event.pathParameters.uuid);
          ret.results.dislike_count = results.Items.length;
          console.log("ret is now: " + JSON.stringify(ret));
      } catch (err) {
          ret.error = err;
          console.log("ret is now: " + JSON.stringify(ret));
      }
    }
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
