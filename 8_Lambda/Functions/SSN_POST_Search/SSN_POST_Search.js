/*
 * SoshulNetwork
 *
 * Description: Search for data in beacons
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

var user_info_cache = [];

const one_day_in_seconds = 86400;


async function getUserInfoForSub(sub) {
  console.log("getUserInfoForSub():: " + sub);
  var ret = {"sub":"", "name":""};
  
  //check local cache prior to querying DDB
  var now = Math.round(Date.now() / 1000); // for age check.
  for (var i = 0; i < user_info_cache.length; i++) {
    if (user_info_cache[i].sub === sub) {
      if (now - user_info_cache[i].cache_time < one_day_in_seconds) {
        ret.sub = user_info_cache[i].sub;
        ret.name = user_info_cache[i].name;
        console.log("getUserInfoForSub():: found a match in cache: " +  (now - user_info_cache[i].cache_time) + " seconds old.");

        return ret;
      } else {
        //we had a match but it's too old, get out of this loop.
        console.log("getUserInfoForSub():: found a match in cache but it's too old: " +  (now - user_info_cache[i].cache_time));
        break;
      }
    }
  }
  
  var user_pool_id = "INSERT_USER_POOL_ID";
  
  var params = {
    Username: sub,
    UserPoolId: user_pool_id
  }
  
  try {
    await cognitoidentityserviceprovider.adminGetUser(params, function(err, data){
      if (err) {
        console.log(err);
      } else {
        console.log("getUserInfoForSub():: RX data from Cognito for user: " + JSON.stringify(data));
        for (var i = 0; i < data.UserAttributes.length; i++) {
          var item = data.UserAttributes[i];
          //Don't leak user's email address info.
          if (!(item.Name === "email") && !(item.Name === "email_verified")) {
            ret[item.Name] = item.Value;
          }
        }
      };
    }).promise();
    
    //store to local cache for future use.
    ret.cache_time = Math.round(Date.now() / 1000)
    user_info_cache.push(ret);
    console.log("getUserInfoForSub():: added user info to the cache. cache is now: " + JSON.stringify(user_info_cache));
    
    return ret;
  } catch (err) {
    return ret;
  }
  
}

async function scan_beacons(search_term) {
  console.log("scan_beacons():: term: " + search_term);
  
  const beacon_scan_params = {
    TableName: "SoshulNetwork",
    FilterExpression: "contains(#beacon_data, :search_term) OR contains(#beacon_title, :search_term)",
    ExpressionAttributeNames: {
        "#beacon_data": "beacon_data",
        "#beacon_title": "beacon_title"
    },
    ExpressionAttributeValues: {
        ":search_term": search_term
    }  
  };

  try {
    const data = await docClient.scan(beacon_scan_params).promise()
    //get user info for each beacon
    for (var i = 0; i < data.Items.length; i++) {
      var user_info = await getUserInfoForSub(data.Items[i].sub_uuid);
      data.Items[i].user_info = user_info;
      console.log("data is now: " + JSON.stringify(data));
    }
    console.log("returning data: " + JSON.stringify(data));
    return data
  } catch (err) {
    console.log("returning error: " + JSON.stringify(err));
    return err
  }
}




exports.handler = async (event, context) => {
    var ret = {"error":"", "results":""};
    
    try {
        var body = JSON.parse(event.body);
    
        if ("search_term" in body) {
            ret.results = await scan_beacons(body.search_term);
        } else {
            ret.error = "Malformed Beacon POST";
        }
    } catch (err) {
      console.log("caught error: " + err);
        ret.error = err;
    }
    
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
