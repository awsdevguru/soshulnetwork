#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_OPTIONS_All.zip SSN_OPTIONS_All.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_OPTIONS_All" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_OPTIONS_All.zip" --runtime "nodejs14.x" --handler "SSN_OPTIONS_All.handler" --tracing-config '{"Mode": "Active"}'

