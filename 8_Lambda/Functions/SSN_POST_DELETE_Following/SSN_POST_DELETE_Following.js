/*
 * SoshulNetwork
 *
 * Description: Add/Delete a uuid from the following array
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

async function postDeleteFollow(sub_uuid, follow_uuid, http_verb){
    console.log("postDeleteFollow():: sub_uuid: " + sub_uuid + " follow_uuid: " + follow_uuid + " Verb: " + http_verb);
    var ret = {"error":"", "result":""};

    //Get the following entry, modify the following attribute array
    //Put back the new data
    
    const query_following_params = {
        TableName : 'SoshulNetworkLive',
        Key: {
          pk: 'following',
          sk: sub_uuid // this is the currently logged in user making the call, the follower
        }
    }
    
    const query_followers_params = {
        TableName : 'SoshulNetworkLive',
        Key: {
          pk: 'followers',
          sk: follow_uuid // this is the sub_uuid of the followee
        }
    }
  
    try {
        const following_data = await docClient.get(query_following_params).promise();
        const followers_data = await docClient.get(query_followers_params).promise();
        console.log("postDeleteFollow():: found following data: " + JSON.stringify(following_data));
        console.log("postDeleteFollow():: found followers data: " + JSON.stringify(followers_data));
        
        // Handle followERS silently
        const followers_item_params = {
              TableName: 'SoshulNetworkLive',
              Item: {
                pk: "followers",
                sk: follow_uuid,
                followers: []
              }
            };
            
        if ("Item" in followers_data) {
          followers_item_params.Item.followers = followers_data.Item.followers;
        }
        
        var push_new_followers = false;
        
        if (http_verb === "POST") {
          //Add follower if not exist
          var index = followers_item_params.Item.followers.indexOf(sub_uuid);
          if (index == -1) {
            console.log("postDeleteFollow(er)():: Adding sub_uuid to followers array");
            followers_item_params.Item.followers.push(sub_uuid);
            push_new_followers = true;
          } else {
            console.log("postDeleteFollow(er)():: users is already a follower, ignoring");
          }
        } else if (http_verb === "DELETE") {
          //delete follower if exist
          var index = followers_item_params.Item.followers.indexOf(sub_uuid);
          if (index >= -1) {
            console.log("postDeleteFollow(er)():: removing sub_uuid from followers array");
            followers_item_params.Item.followers.splice(index, 1);
            push_new_followers = true;
          } else {
            console.log("postDeleteFollow(er)():: users wasn't a follower, ignoring");
          }
        }
        //push followers back to DDB
        if (push_new_followers) {
          console.log("postDeleteFollow(er)():: pushing followers Item: " + JSON.stringify(followers_item_params));
          await docClient.put(followers_item_params).promise();
        }
        
        
        // Handle followING 
        if ("Item" in following_data) {
            //following exists, add or delete based on HTTP verb
            
            if (http_verb === "POST") {
              //loop over existing following for duplicate, if not found, add
              console.log("postDeleteFollow():: http_verb is POST, checking for dupe and adding.");
              
              if (following_data.Item.following.indexOf(follow_uuid) > -1) {
                ret.error = "Already following.";
                return ret;
              }
              
              //Add followee to array
              following_data.Item.following.push(follow_uuid);
              console.log("data is now: " + JSON.stringify(following_data));
              
            } else if (http_verb === "DELETE") {
              //find the followee and remove it
              console.log("postDeleteFollow():: http_verb is DELETE, finding index of followee to delete.");
              var index = following_data.Item.following.indexOf(follow_uuid);

              if (index >= 0) {
                following_data.Item.following.splice(index, 1);
                console.log("postDeleteFollow():: removed item from following array, now: " + JSON.stringify(following_data));
              } else {
                ret.error = "Followee not found.";
                return ret;
              }
            }
            
            // push the updated following object back to DDB
            const item_params = {
              TableName: 'SoshulNetworkLive',
              Item: following_data.Item
            }
            
            try {
                console.log("postDeleteFollow():: updating following entry in table: " + JSON.stringify(item_params));
                await docClient.put(item_params).promise();
                ret.result = "Following list updated.";
                return ret;
            } catch (err) {
                console.log("postDeleteFollow():: caught error: " + err);
                ret.error = err;
                return ret;
            }
        } else {
            //make a new following object in DDB
            console.log("postDeleteFollow():: following not exist, making new");
            // push the updated following object back to DDB
            const item_params = {
              TableName: 'SoshulNetworkLive',
              Item: {
                pk: "following",
                sk: sub_uuid,
                following: [follow_uuid]
              }
            }
            
            try {
                console.log("postDeleteFollow():: adding new following entry in table: " + JSON.stringify(item_params));
                await docClient.put(item_params).promise();
                ret.result = "Following list updated.";
                return ret;
            } catch (err) {
                console.log("postDeleteFollow():: caught error: " + err);
                ret.error = err;
                return ret;
            }
        }
    
    } catch (err) {
        console.log("postDeleteFollow():: caught error: " + JSON.stringify(err));
        ret.error = err.error;
        return ret;
    }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "result":""};
    
    try {
        if ("sub" in event.pathParameters) {
            if (event.httpMethod === "POST" || event.httpMethod === "DELETE") {
                ret = await postDeleteFollow(event.requestContext.authorizer.claims.sub, event.pathParameters.sub, event.httpMethod);
            } else {
                ret.error = "Bad verb.";
            }
        } else {
            ret.error = "Malformed Follow";
        }
    } catch (err) {
        console.log("handler():: caught error: " + err);
        ret.error = err;
    }
    
    
    console.log("handler():: returning response: " + JSON.stringify(ret));
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};

