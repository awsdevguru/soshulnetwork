const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

var params = {
  TableName: 'SoshulNetworkLive',
  KeyConditionExpression: '#name = :value',
  ExpressionAttributeValues: { ':value': 'beacons' },
  ExpressionAttributeNames: { '#name': 'pk' }
}

async function queryBeacons(){
  console.log("queryBeacons()");
  try {
    const data = await docClient.query(params).promise()
    console.log("returning data: " + JSON.stringify(data));
    return data
  } catch (err) {
    console.log("returning error: " + JSON.stringify(err));
    return err
  }
}
exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
    
    try {
        ret.results = await queryBeacons()
        console.log("ret is now: " + JSON.stringify(ret));
    } catch (err) {
        ret.error = err;
        console.log("ret is now: " + JSON.stringify(ret));
    }
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
