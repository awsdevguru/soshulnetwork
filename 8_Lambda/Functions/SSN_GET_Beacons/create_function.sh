#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_GET_Beacons.zip SSN_GET_Beacons.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_GET_Beacons" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_GET_Beacons.zip" --runtime "nodejs14.x" --handler "SSN_GET_Beacons.handler" --tracing-config '{"Mode": "Active"}'

echo "NOTE: you need to add the XRay layer to this function in the Web Console."
