/*
 * SoshulNetwork
 *
 * Description: Delete a beacon by UUID
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 * NOTE: doesn't verify ownership, oops
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();


async function deleteBeacon(uuid){
  var now = Date.now();
  const params = {
    TableName : 'SoshulNetworkLive',
    Key: {
       pk: 'beacons',
       sk: uuid
    }
  }
  
  try {
    await docClient.delete(params).promise();
  } catch (err) {
    return err;
  }
}
exports.handler = async (event, context) => {
    var ret = {"error":"", "result":""};
    
    try {
        if ("uuid" in event.pathParameters) {
            const result = await deleteBeacon(event.pathParameters.uuid);
            ret.result = "Beacon Deleted";
        } else {
            ret.error = "Malformed Beacon DELETE";
        }
    } catch (err) {
        ret.error = err;
    }
    
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};

