/*
 * SoshulNetwork
 *
 * Description: Add a Like count for a beacon
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();


async function postLike(uuid, beacon_uuid, sub_uuid){
    var ret = {"error":"", "result":""};
    
    //Check for beacon existance before posting the Like
    const query_beacon_params = {
        TableName : 'SoshulNetworkLive',
        Key: {
          pk: 'beacons',
          sk: beacon_uuid
        }
    }
  
    try {
        const beacon_data = await docClient.get(query_beacon_params).promise();
        console.log("postLike():: found beacon data: " + JSON.stringify(beacon_data));
        
        if ("Item" in beacon_data) {
            //beacon exists, add the Like
            var now = Date.now();
            const like_params = {
                TableName : 'SoshulNetworkLive',
                Item: {
                   pk: 'likes',
                   sk: uuid,
                   beacon_uuid: beacon_uuid,
                   sub_uuid: sub_uuid
                }
            }
            
            try {
                console.log("postLike():: adding like to table: " + JSON.stringify(like_params));
                await docClient.put(like_params).promise();
                ret.result = "Like posted";
                return ret;
            } catch (err) {
                console.log("postLike():: returning error: " + err);
                ret.error = err;
                return ret;
            }
        } else {
            console.log("beacon not exist");
            ret.error = "Beacon not found.";
            return ret;
        }
    
    } catch (err) {
        ret.error = err;
        return ret;
    }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "result":""};
    
    try {
        if ("uuid" in event.pathParameters) {
            ret = await postLike(context.awsRequestId, event.pathParameters.uuid, event.requestContext.authorizer.claims.sub);
        } else {
            ret.error = "Malformed Like POST";
        }
    } catch (err) {
        ret.error = err;
    }
    
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
