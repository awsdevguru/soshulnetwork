#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_POST_Like.zip SSN_POST_Like.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_POST_Like" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_POST_Like.zip" --runtime "nodejs14.x" --handler "SSN_POST_Like.handler" --tracing-config '{"Mode": "Active"}'

