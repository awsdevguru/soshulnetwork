#!/bin/sh
#
# NOTE: You must edit the account number in the --role option
#
echo Packaging
rm *.zip
zip SSN_DELETE_Beacons.zip SSN_DELETE_Beacons.js

# Create Function
echo "Create Function"
aws lambda create-function --function-name "SSN_DELETE_Beacons" --role "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live" --zip-file "fileb://SSN_DELETE_Beacons.zip" --runtime "nodejs14.x" --handler "SSN_DELETE_Beacons.handler" --tracing-config '{"Mode": "Active"}'

