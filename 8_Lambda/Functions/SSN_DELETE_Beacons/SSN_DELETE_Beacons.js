/*
 * SoshulNetwork
 *
 * Description: Delete all beacons published by the logged in user.
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();


async function deleteBeacon(sub, uuid){
  var now = Date.now();
  const params = {
    TableName : 'SoshulNetworkLive',
    Key: {
       pk: 'beacons',
       sk: uuid
    },
    ConditionExpression:"sub_uuid = :val",
    ExpressionAttributeValues: {
        ":val": sub
    }
  }
  
  try {
    var result = await docClient.delete(params).promise();
    console.log(result);
  } catch (err) {
    console.log("returnning error: " + err);  
    return err;
  }
}
exports.handler = async (event, context) => {
    var ret = {"error":"", "result":""};
    
    
    try {
        var uuids = JSON.parse(event.body);
        console.log(uuids); // [<uuid>,<uuid>]
        if ("sub" in event.requestContext.authorizer.claims) {
            for ( var i = 0; i < uuids.length; i++) {
                const result = await deleteBeacon(event.requestContext.authorizer.claims.sub, uuids[i]);
                console.log("deleted beacon with uuid: " + uuids[i]);
                console.log(result);
            }
            ret.result = "Beacons Deleted";
        } else {
            ret.error = "Malformed Beacon DELETE";
        }
    } catch (err) {
        ret.error = err;
    }
    
    
    console.log("returning response");
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};

