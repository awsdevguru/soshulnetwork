/*
 * SoshulNetwork
 *
 * Description: Get a list of users the this user is following
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

async function getFollowing(sub){
  var ret = {"error":"", "results":{}};
  
  console.log("getFollowing():: sub: " + sub);
  const params = {
    TableName : 'SoshulNetworkLive',
    Key: {
      pk: 'following',
      sk: sub
    }
  }

  try {
    const data = await docClient.get(params).promise();
    console.log("getFollowing():: the data: " + JSON.stringify(data));
    if ("Item" in data) {
      ret.results = data.Item;
    } else {
      //Forge response
      ret.results.following = [];
      ret.results.sk = sub;
      ret.results.pk = "following";
      ret.error = ""; // it's not found but we don't need an error, 
                      // just means no followings for this user.
    }
    return ret;
  } catch (err) {
    ret.error = err;
    return ret;
  }
}

exports.handler = async (event, context) => {
    var ret = {"error":"", "results":{}};
    console.log(JSON.stringify(event));
  
    var sub = event.pathParameters.sub;
    
    try {
        ret = await getFollowing(sub);
    } catch (err) {
        console.log("error: " + err);
        ret.error = err;
    }

    console.log("SSN_GET_Following():: ret: " + JSON.stringify(ret));
    
    const response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'https://live.soshul.network'
        },
        body: JSON.stringify(ret)
    };
    return response;
};
