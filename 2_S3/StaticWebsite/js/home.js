/*
 * SoshulNetwork
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

function build_page() {
    get_current_user_info();
    get_latest_beacons();
    $("#loading_overlay").hide();

    $("#search_form").submit(function(evt){
      event.preventDefault();
      verify_search();
    });
    
    $("#btn_navbar_search").click(function() {
      verify_search();
    });
}

function verify_search() {
  var search_term = $("#navbar_search_term").val();
  console.log("search clicked, value: " + search_term);
  if (search_term.length > 0) {
    do_search(search_term);
  } else {
    toastr.warning("No search term.");
  }
}


function do_search(query) {
  //should call a lambda function that does a scan of beacon_text
  // toastr.error("Not implemented yet.");
  $("#latest_beacon_column_0").empty();
  $("#latest_beacon_column_1").empty();
  $("#latest_beacon_column_2").empty();
  $("#latest_beacon_column_3").empty();

  /*
  var url = apig_url + "/beacons";
  console.log("get_latest_beacons():: querying api/beacons");
  $.getJSON( url, function( data ) {
    console.log("get_latest_beacons():: beacon count: " + data.results.Items.length);
    $("#beacon_count").text("(" + data.results.Items.length + ")");
    console.log("get_latest_beacons():: data: " + JSON.stringify(data));
    if (data.error.length > 0) {
      toastr.error(data.error);
    } else {
      console.log("get_latest_beacons():: beacons data: " + JSON.stringify(data));
      // for each returned beacon, append to the UI
      for (var i = 0; i < data.results.Items.length; i++) {
        var column_to_append_to = i % 4;
        append_beacon(data.results.Items[i], column_to_append_to);
      }
    }
  })
  .fail(function(err) {
    console.log( "get_latest_beacons():: error: " + JSON.stringify(err) );
    toastr.error("Failed to get latest beacons.");
  });*/

  var url = apig_url + "/beacons/search";

  var post = {"search_term":query};

  $.ajax({
    type: "POST",
    url: url,
    data: JSON.stringify(post),
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      console.log("do_search():: beacon count: " + data.results.Items.length);
      $("#page_title").text("Search: \"" + query + "\" (" + data.results.Items.length + ")");
      console.log("do_search():: data: " + JSON.stringify(data));
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        console.log("do_search():: beacons data: " + JSON.stringify(data));
        // for each returned beacon, append to the UI
        for (var i = 0; i < data.results.Items.length; i++) {
          var column_to_append_to = i % 4;
          append_beacon(data.results.Items[i], column_to_append_to);
        }
      }
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}


function get_latest_beacons() {
  
    $("#latest_beacon_column_0").empty();
    $("#latest_beacon_column_1").empty();
    $("#latest_beacon_column_2").empty();
    $("#latest_beacon_column_3").empty();

    var url = apig_url + "/beacons";
    console.log("get_latest_beacons():: querying api/beacons");
    $.getJSON( url, function( data ) {
      console.log("get_latest_beacons():: beacon count: " + data.results.Items.length);
      $("#page_title").text("Latest Beacons (" + data.results.Items.length + ")");
      console.log("get_latest_beacons():: data: " + JSON.stringify(data));
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        console.log("get_latest_beacons():: beacons data: " + JSON.stringify(data));
        // for each returned beacon, append to the UI
        for (var i = 0; i < data.results.Items.length; i++) {
          var column_to_append_to = i % 4;
          append_beacon(data.results.Items[i], column_to_append_to);
        }
      }
    })
    .fail(function(err) {
      console.log( "get_latest_beacons():: error: " + JSON.stringify(err) );
      toastr.error("Failed to get latest beacons.");
    });
}

function append_beacon(beacon, column) {
  console.log("append_beacon():: beacon: " + JSON.stringify(beacon) + " column: " + column);

  var column_to_append_to = "#latest_beacon_column_" + column;

  if (beacon.beacon_type == beacon_type.text) {
    //text beacon type
    var beacon_card = make_beacon_text_summary_card(beacon);
    $(column_to_append_to).append(beacon_card);
  } else if (beacon.beacon_type == beacon_type.img) {
    //img beacon type
    var beacon_card = make_beacon_img_summary_card(beacon);
    $(column_to_append_to).append(beacon_card);
  }   
}

function post_new_beacon_from_modal() {
  console.log("post_new_beacon_from_modal():: start");
  // 
  var new_beacon_type = $("input[name='beacon_type_options']:checked").val();
  var new_beacon_color = $("input[name='beacon_color_options']:checked").val();
  var new_beacon_title = $("#beacon_title").val();
  var new_beacon_data = $("#beacon_data").val();
  console.log("New beacon_type: " + new_beacon_type);
  console.log("New beacon_color: " + new_beacon_color);
  console.log("New beacon_title: " + new_beacon_title);
  console.log("New beacon_data: " + new_beacon_data);

  if (new_beacon_data.length == 0) { toastr.error("Error: missing beacon data."); }
  else if (new_beacon_title.length == 0) { toastr.error("Error: missing beacon title."); }
  else if (new_beacon_color.length == 0) { toastr.error("Error: missing beacon data."); }
  else { 

    var beacon_to_post = { 
      "beacon_data":new_beacon_data, 
      "beacon_type":0, 
      "beacon_color":new_beacon_color, 
      "beacon_title":new_beacon_title
    }

    if (new_beacon_type === "beacon_type_img") {
      //change type if image beacon
      beacon_to_post.beacon_type = beacon_type.img; 
      if (beacon_to_post.beacon_data.indexOf("http") != 0) {
        toastr.error("Image beacon URL must be HTTP/S.");
        return;
      }
    }

    post_beacon(beacon_to_post);

    $("#newBeaconModal").modal('hide');
  }
}

$( document ).ready(function() {
    console.log("ssn.js ready");
  
    init();
  
    setTimeout(check_auth, 500);
  
    $("#btn_compose_beacon").click(function() {
      console.log("btn_compose_beacon clicked");
      $("#newBeaconModal").modal('show');
      $("#beacon_title").val("");
      $("#beacon_data").val("");
    });
  
    $("#btn_close_compose_beacon").click(function() {
      console.log("btn_close_compose_beacon clicked");
      $("#newBeaconModal").modal('hide');
    });
  
    $("#btn_send_beacon").click(function() {
      console.log("btn_send_beacon clicked");
      post_new_beacon_from_modal();
    });

    $('input[type=radio][name=beacon_type_options]').change(function() {
      if (this.value == 'beacon_type_text') {
          console.log("new beacon, change type to text");
          $("#beacon_data_title").text("Message");
      }
      else if (this.value == 'beacon_type_img') {
        console.log("new beacon, change type to img");
        $("#beacon_data_title").text("Image URL (Fully qualified)");
      }
  });
  
  });