/*
 * SoshulNetwork
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

var id_token = null;

const beacon_type = {
  "text": 0,
  "img": 1
}

const like_types = {
  "like": 1,
  "dislike": 0
}

var apig_url = "INSERT_API_GATEWAY_URL"
var cognito_login_url = "INSERT_COGNITO_LOGIN_URL"

var is_authenticated = false;
var user_info = null;

var cached_user_info = [];

/*
 * Perform a simple call to API gateway to get a valid/known response or not.
 * Uses a Mock integration in API Gateway
 * If not, redirect to Cognito Hosted UI Login
 */
function check_auth() {
  console.log("check_auth():: querying api/authenticated");
  var url = apig_url + "/authenticated";
  $.getJSON( url, function( data ) {
    console.log("check_auth():: " + JSON.stringify(data));
    is_authenticated = true;
    build_page(); // function should exist in per-page JS
  })
  .fail(function(err) {
    console.log( "check_auth():: error: " + JSON.stringify(err) );
    is_authenticated = false;
    toastr.error("Not logged in...");
    window.location = cognito_login_url;
  });
}

function make_beacon_text_summary_card(beacon) {
  console.log("make_beacon_text_summary_card():: beacon: " + JSON.stringify(beacon));
  var text_template = '<div class="card bg-#COLOR# text-white mb-4"> <div class="card-body">' +
                      '<h5 class="card-title">#TITLE#</h5>' +
                      '<p class="card-text">#CONTENT#</p>' +
                      '<small>#AUTHOR#</small></div>' +
                      '<div class="card-footer d-flex align-items-center justify-content-between">' +
                      '<a class="small text-white stretched-link" href="/beacon.html#uuid=#UUID#">View Details</a> <div class="small text-white"><i class="fas fa-angle-right"></i></div>' +
                      '</div></div>'


  var beacon_card = text_template.replace("#COLOR#", beacon.beacon_color)
                                  .replace("#CONTENT#", beacon.beacon_data)
                                  .replace("#TITLE#", beacon.beacon_title)
                                  .replace("#UUID#", beacon.sk)
                                  .replace("#AUTHOR#", beacon.user_info.name + " | " + moment(beacon.published_at).format('MMMM Do YYYY, h:mm:ss a'));
    
    return beacon_card;
}

function make_beacon_img_summary_card(beacon) {
  console.log("make_beacon_img_summary_card():: beacon: " + JSON.stringify(beacon));
  var img_template = ' <div class="card bg-#COLOR# text-white mb-4"> <div class="card-body">' +
                     '<h5 class="card-title">#TITLE#</h5>' +
                     '<p class="card-text">#CONTENT#</p>' +
                     '<small>#AUTHOR#</small></div>' +
                     '<div class="card-footer d-flex align-items-center justify-content-between">' +
                     '<a class="small text-white stretched-link" href="/beacon.html#uuid=#UUID#">View Details</a> <div class="small text-white"><i class="fas fa-angle-right"></i></div>' +
                     '</div></div>'

  var beacon_card = img_template.replace("#COLOR#", beacon.beacon_color)
                .replace("#CONTENT#", '<img src="'+beacon.beacon_data+'" style="width: 100%;">')
                .replace("#TITLE#", beacon.beacon_title)
                .replace("#UUID#", beacon.sk)
                .replace("#AUTHOR#", beacon.user_info.name + " | " + moment(beacon.published_at).format('MMMM Do YYYY, h:mm:ss a'));
  
  return beacon_card;
}

function make_beacon_text_detail_card(beacon) {
  console.log("make_beacon_text_detail_card():: beacon: " + JSON.stringify(beacon));
  var text_template = '<div class="card mb-4"> <div class="card-body">' +
                      '<h5 class="card-title">#TITLE#</h5>' +
                      '<h6 class="card-subtitle mb-2 text-muted">#SUBTITLE#</h6>' +
                      '<p class="card-text">#CONTENT#</p>' +
                      '<div class="card-footer bg-#COLOR#">' +
                      '<div class="row">' +
                        '<div class="col-sm-6"><center><i class="fas fa-thumbs-down" id="btn_dislike" onClick="click_like_dislike(like_types.dislike)"></i> <span id="dislike_count">-</span> </center></div>' +
                        '<div class="col-sm-6"><center><i class="fas fa-thumbs-up" id="btn_like" onClick="click_like_dislike(like_types.like)"></i> <span id="like_count">-</span> </center></div>'+
                      '</div></div>' +
                      '</div></div>'


  var beacon_card = text_template.replace("#COLOR#", beacon.beacon_color)
                                  .replace("#CONTENT#", beacon.beacon_data)
                                  .replace("#TITLE#", beacon.beacon_title)
                                  .replace("#UUID#", beacon.sk)
                                  .replace("#SUBTITLE#", beacon.user_info.name + " | " + moment(beacon.published_at).format('MMMM Do YYYY, h:mm:ss a'));
    return beacon_card;
}

function make_beacon_img_detail_card(beacon) {
  console.log("make_beacon_img_detail_card():: beacon: " + JSON.stringify(beacon));
  var img_template = '<div class="card mb-4"> <div class="card-body">' +
                     '<h5 class="card-title">#TITLE#</h5>' +
                     '<h6 class="card-subtitle mb-2 text-muted">#SUBTITLE#</h6>' +
                     '<p class="card-text">#CONTENT#</p>' +
                     '<div class="card-footer bg-#COLOR#">' +
                     '<div class="row">' +
                       '<div class="col-sm-6"><center><i class="fas fa-thumbs-down" id="btn_dislike" onClick="click_like_dislike(like_types.dislike)"></i> <span id="dislike_count">-</span></center></div>' +
                       '<div class="col-sm-6"><center><i class="fas fa-thumbs-up" id="btn_like" onClick="click_like_dislike(like_types.like)"></i> <span id="like_count">-</span> </center></div>'+
                     '</div></div>' +
                     '</div></div>'

  var beacon_card = img_template.replace("#COLOR#", beacon.beacon_color)
                .replace("#CONTENT#", '<img src="'+beacon.beacon_data+'" style="width: 100%;">')
                .replace("#TITLE#", beacon.beacon_title)
                .replace("#UUID#", beacon.sk)
                .replace("#SUBTITLE#", beacon.user_info.name + " | " + moment(beacon.published_at).format('MMMM Do YYYY, h:mm:ss a'));
  return beacon_card;
}


function setup_toastr() {
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };
}

function get_current_user_info() {
  var url = apig_url + "/user";
  console.log("get_current_user_info():: querying api/user");
  $.getJSON( url, function( data ) {
    console.log("get_current_user_info():: data: " + JSON.stringify(data));
    if (data.error.length > 0) {
      toastr.error(data.error);
    } else {
      user_info = data.results;
      update_username_in_nav_from_user_info();
    }
  })
  .fail(function(err) {
    console.log( "update_username_in_nav():: error: " + JSON.stringify(err) );
    toastr.error("Failed to get user info");
  });
}

function update_username_in_nav_from_user_info() {
  if (user_info != null) {
    $("#user_full_name").html("<b>" + user_info.name + "</b>");
    $("#menu_user_full_name").html("<b>" + user_info.name + "</b>");
    console.log("update_username_in_nav_from_user_info():: " + user_info.name);
  }
}

function init() {
  id_token = localStorage.getItem("id_token");
  console.log("id_token: " + id_token);

  if (id_token != null) {
    console.log("$.ajaxSetup");
    $.ajaxSetup({
      headers : {
        'Authorization' : id_token
      }
    });
  }

  setup_toastr();

  $("#logout_button").click(function() {
    console.log("logout clicked");
    localStorage.setItem("id_token", "");
    location.reload();
  });
}



function post_beacon(beacon_to_post) {
  //toastr.info("Posting Beacon: " + JSON.stringify(beacon_to_post));
  console.log("post_beacon():: " + beacon_to_post.beacon_type + ", " + beacon_to_post.beacon_title + ", " + beacon_to_post.beacon_data + ", " + beacon_to_post.beacon_color);
  
  var url = apig_url + "/beacon";

  $.ajax({
    type: "POST",
    url: url,
    data: JSON.stringify(beacon_to_post),
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      console.log(data);
      toastr.info(data.result);
      get_latest_beacons();
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}

function delete_beacon(beacon_uuid) {
  console.log("delete_beacon():: uuid: " + beacon_uuid);
  
  var url = apig_url + "/beacon/"+beacon_uuid;
  toastr.info("Deleting Beacon: " + beacon_uuid);
  
  var url = apig_url + "/beacon/" + beacon_uuid;
  console.log("delete_beacon():: url: " + url);
  var beacon_to_delete = { "beacon_uuid": beacon_uuid };

  $.ajax({
    type: "DELETE",
    url: url,
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        toastr.success(data.result);
        get_my_beacons();
      }
      console.log(data);
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}

function delete_all_beacons(uuids) {
  console.log("delete_all_beacons():: start");
  
  var url = apig_url + "/beacons";
  toastr.info("Deleting all Beacons!");
  
  var url = apig_url + "/beacons";
  console.log("delete_all_beacons():: url: " + url);

  $.ajax({
    type: "DELETE",
    url: url,
    data: JSON.stringify(uuids),
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        toastr.success(data.result);
        get_my_beacons();
      }
      console.log(data);
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}



function post_like_dislike(beacon_uuid, like_type) {
  console.log("post_like_dislike():: " + beacon_uuid + ", " + like_types[like_type]);
  
  var url = "";
  if (like_type == like_types.like) {
    url = apig_url + "/beacon/"+beacon_uuid+"/like";
  } else if (like_type == like_types.dislike) {
    url = apig_url + "/beacon/"+beacon_uuid+"/dislike";
  } else {
    toastr.error("Unknown like type.");
    return;
  }

  var data_to_post = {};

  $.ajax({
    type: "POST",
    url: url,
    data: JSON.stringify(data_to_post),
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      console.log(data);
      toastr.info(data.result);
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}
