/*
 * SoshulNetwork
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

var beacon_uuid = "";
var beacon = null;

function build_page() {
    var hash = window.location.hash;
    var idx = hash.indexOf('uuid');

    get_current_user_info();

    if (idx >= 0) {
      // has '#uuid' in URL
      var uuid_str = hash.substring(idx);
      beacon_uuid = uuid_str.substring(uuid_str.indexOf('=') + 1).trim();
      if (beacon_uuid !== "") {
        console.log("Received uuid: " + beacon_uuid);
        get_beacon_by_uuid(beacon_uuid);
      } else {
        console.error("build_page():: Missing uuid value.");
        toastr.error("Missing UUID.");
      }
    } else {
      console.error("build_page():: Missing UUID.");
      toastr.error("Missing UUID");
    }

    $("#loading_overlay").hide();
}

function click_like_dislike(like_type) {
  console.log("click_like_dislike():: " + like_type);
  post_like_dislike(beacon_uuid, like_type);
  setTimeout(function() {get_beacon_dislike_count(beacon_uuid);}, 1500);
  setTimeout(function() {get_beacon_like_count(beacon_uuid);}, 1500);
}

function get_beacon_by_uuid(uuid) {
  var url = apig_url + "/beacon/"+uuid;
  console.log("get_beacon_by_uuid():: url: " + url);
  console.log("get_beacon_by_uuid():: querying api/beacon/"+uuid);
  $.getJSON( url, function( data ) {
    console.log("get_beacon_by_uuid():: data: " + JSON.stringify(data));
    if (data.error.length > 0) {
      toastr.error(data.error);
    } else {
      console.log("get_beacon_by_uuid():: beacon data: " + JSON.stringify(data));
      beacon = data.results.Item;
      $("#page_header").text(beacon.user_info.name + "'s Beacon");
      $("#page_header_date").text( moment(beacon.published_at).format('MMMM Do YYYY, h:mm:ss a') );
      
      var beacon_card = "<p>Error</p>";
      if (beacon.beacon_type == beacon_type.text) {
        beacon_card = make_beacon_text_detail_card(beacon);
      } else if (beacon.beacon_type == beacon_type.img) {
        beacon_card = make_beacon_img_detail_card(beacon);
      }

      $("#the_beacon").append(beacon_card);

      get_beacon_dislike_count(uuid);
      get_beacon_like_count(uuid);
      setTimeout(function() {update_following(beacon.user_info.sub, beacon.user_info.name);}, 1000); // this is delayed because of a race condition with get_current_user_info up there :pointup:
    }
  })
  .fail(function(err) {
    console.log( "get_beacon_by_uuid():: error: " + JSON.stringify(err) );
    toastr.error("Failed to get beacon: " + uuid);
  });
}

function get_beacon_dislike_count(uuid) {
  console.log("get_beacon_dislike_count():: uuid: " + uuid);
  var url = apig_url + "/beacon/"+uuid+"/dislike";
  $.getJSON( url, function( data ) {
    if (data.error.length > 0) {
      toastr.error(data.error);
    } else {
      console.log("get_beacon_dislike_count():: beacon data: " + JSON.stringify(data));
      var count = data.results.dislike_count;
      $("#dislike_count").text(count);
    }
  })
  .fail(function(err) {
    console.log( "get_beacon_dislike_count():: error: " + JSON.stringify(err) );
    toastr.error("Failed to get dislikes: " + uuid);
  });
}

function get_beacon_like_count(uuid) {
  console.log("get_beacon_like_count():: uuid: " + uuid);
  var url = apig_url + "/beacon/"+uuid+"/like";
  $.getJSON( url, function( data ) {
    if (data.error.length > 0) {
      toastr.error(data.error);
    } else {
      console.log("get_beacon_like_count():: beacon data: " + JSON.stringify(data));
      var count = data.results.like_count;
      $("#like_count").text(count);
    }
  })
  .fail(function(err) {
    console.log( "get_beacon_like_count():: error: " + JSON.stringify(err) );
    toastr.error("Failed to get likes: " + uuid);
  });
}

function update_following(poster_sub, poster_name) {
  console.log("update_following():: sub: " + user_info.sub + " poster_sub: " + poster_sub);

  if (poster_sub === user_info.sub) {
    console.log("update_following():: my beacon, not checking following info.");
    return;
  }
  

  var url = apig_url + "/user/"+user_info.sub+"/following";
  console.log("update_following():: url: " + url);
  console.log("update_following():: querying api/user/sub/following");
  $.getJSON( url, function( data ) {
    console.log("update_following():: data: " + JSON.stringify(data));
    if (data.error.length > 0) {
      toastr.error(data.error);
    } else {
      console.log("success");
      var found = false;


      if (data.results.following.indexOf(poster_sub) > -1) {
        console.log("update_following():: matched following sub: " + poster_sub);
        $("#page_header_following").html('<a href="javascript:unfollow_sub(\''+poster_sub+'\')">Unfollow ' + poster_name + '</a>');
      } else {
        $("#page_header_following").html('<a href="javascript:follow_sub(\''+poster_sub+'\')">Follow ' + poster_name + '</a>');
      }

      // for (var i = 0; i < data.results.following.length; i++) {
      //   var f = data.results.following[i];
      //   if (f === poster_sub) {
      //     console.log("update_following():: matched following sub: " + poster_sub);
      //     $("#page_header_following").html('<a href="javascript:unfollow_sub(\''+poster_sub+'\')">Unfollow ' + poster_name + '</a>');
      //     found = true;
      //     break;
      //   }
      // }

      // if (!found) {
      //   $("#page_header_following").html('<a href="javascript:follow_sub(\''+poster_sub+'\')">Follow ' + poster_name + '</a>');
      // }
    }
  })
  .fail(function(err) {
    console.log( "update_following():: error: " + JSON.stringify(err) );
    toastr.error("Failed to get following information.");
  });
}

function follow_sub(sub_uuid) {
  console.log("follow_sub():: " + sub_uuid);
  //POST to apig/{current_user_sub}/following
  var url = apig_url + "/user/"+sub_uuid+"/following";

  $.ajax({
    type: "POST",
    url: url,
    data: {},
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      console.log(data);
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        toastr.info(data.result);
        update_following(beacon.user_info.sub, beacon.user_info.name);
      }
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });

}

function unfollow_sub(sub_uuid) {
  console.log("unfollow_sub():: " + sub_uuid);
  //DELETE to apig/{current_user_sub}/following
  var url = apig_url + "/user/"+sub_uuid+"/following";

  $.ajax({
    type: "DELETE",
    url: url,
    data: {},
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      console.log(data);
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        toastr.info(data.result);
        update_following(beacon.user_info.sub, beacon.user_info.name);
      }
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}

$( document ).ready(function() {
    console.log( "beacon.js ready" );
    
    init();
    setTimeout(check_auth, 500);
});