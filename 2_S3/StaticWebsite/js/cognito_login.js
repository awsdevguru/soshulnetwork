/*
 * SoshulNetwork
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

$( document ).ready(function() {
    console.log( "cognito_login.js ready" );

    var hash = window.location.hash;
    var idx = hash.indexOf('id_token');

    if (idx >= 0) {
      // has '#access_token' in URL
      var at_str = hash.substring(idx);
      var id_token = at_str.substring(at_str.indexOf('=') + 1, at_str.indexOf('&')).trim();
      if (id_token !== "") {
        // we're good to go
        console.log("Received id_token: " + id_token +" Storing to localStorage");
        localStorage.setItem("id_token", id_token);
        window.location = "https://live.soshul.network"
      } else {
        console.error('Missing id_token value.');
      }
    } else {
      console.error('Missing id_token.');
      toastr.error("Error: Missing id_token");
    }
});
