/*
 * SoshulNetwork
 *
 * Copyright (c) Nick Garner and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 * 
 * Purposefully verbose and console.log talkative.
 * 
 */

var my_beacons_datatable = null;
var all_my_beacon_uuids = [];

function build_page() {
    my_beacons_datatable = $("#my_beacons_table").DataTable();
    get_current_user_info();
    get_my_beacons();
    $("#loading_overlay").hide();
}

function do_search(query) {
    //should call a lambda function that does a scan of beacon_text
    toastr.error("Not implemented yet.");
}

function get_my_beacons() {
    all_my_beacon_uuids = []; //make sure it's empty
    var url = apig_url + "/beacons/mine";
    console.log("get_my_beacons():: querying api/beacons/mine");
    $.getJSON( url, function( data ) {
      console.log("get_my_beacons():: beacon count: " + data.results.Items.length);
      console.log("get_my_beacons():: data: " + JSON.stringify(data));
      if (data.error.length > 0) {
        toastr.error(data.error);
      } else {
        console.log("get_my_beacons():: beacons data: " + JSON.stringify(data));
        clear_beacons_datatable();
        // for each returned beacon, get it and add it to the table
        for (var i = 0; i < data.results.Items.length; i++) {
            get_beacon_and_add_to_table(data.results.Items[i].sk);
            all_my_beacon_uuids.push(data.results.Items[i].sk);
        }
      }
    })
    .fail(function(err) {
      console.log( "get_my_beacons():: error: " + JSON.stringify(err) );
      toastr.error("Failed to get your beacons.");
    });
}

function clear_beacons_datatable() {
    my_beacons_datatable.rows().clear().draw();
}

function get_beacon_and_add_to_table(beacon_uuid) {    
    console.log("append_bget_beacon_and_add_to_tableeacon():: uuid: " + beacon_uuid);
    var url = apig_url + "/beacon/"+beacon_uuid;
    
    console.log("get_beacon_and_add_to_table():: url: " + url);

    $.getJSON( url, function( data ) {
        console.log("get_beacon_and_add_to_table():: data: " + JSON.stringify(data));
        if (data.error.length > 0) {
            toastr.error(data.error);
        } else {
            console.log("get_beacon_by_uuid():: beacon data: " + JSON.stringify(data));
            var beacon = data.results.Item;
            console.log(beacon);
            var beacon_type_string = "Text";
            if (beacon.beacon_type == 1) {
                beacon_type_string = "IMG";
            }
            var append = [beacon.sk, beacon_type_string, beacon.beacon_title, beacon.beacon_data, '<a href="javascript:delete_beacon(\'' + beacon.sk + '\');">Delete</a>'];
            console.log(append);
            my_beacons_datatable.row.add(append).draw(false);        
        }
    })
    .fail(function(err) {
        console.log( "get_beacon_and_add_to_table():: error: " + JSON.stringify(err) );
        toastr.error("Failed to get beacon: " + uuid);
    });
}




$( document ).ready(function() {
    console.log("ssn.js ready");
  
    init();
  
    setTimeout(check_auth, 500);

    $("#btn_delete_all_beacons").click(function() {
        console.log("btn_delete_all_beacons clicked");
        if (all_my_beacon_uuids.length == 0) {
            toastr.info("No beacons to delete.");
        } else {
            $("#deleteBeaconsModal").modal('show');
        }
    });

    $("#btn_close_delete_beacons").click(function() {
        console.log("btn_close_delete_beacons clicked");
        $("#deleteBeaconsModal").modal('hide');
      });
    
      $("#btn_delete_all_beacons_confirm").click(function() {
        console.log("btn_delete_all_beacons_confirm clicked");
        delete_all_beacons(all_my_beacon_uuids);
        $("#deleteBeaconsModal").modal('hide');
      });
  
  });