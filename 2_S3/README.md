# S3

## Static Website (Modified SB Admin)

### Prep
Modify StaticWebsite/js/ssn.js, apig_url to point to the REST API Gateway that was created.

### S3 Operations

**Make Bucket**
```
$ ./s3_make_bucket_website.sh
> aws s3 mb s3://soshulnetwork-web-live
make_bucket: soshulnetwork-web-live
> aws s3api put-bucket-versioning --bucket soshulnetwork-web-live --versioning-configuration '{ MFADelete: Disabled, Status: Enabled }'
$
```

**Copy website files**
```
$ ./s3_sync_website.sh
> aws s3 sync ./StaticWebsite s3://soshulnetwork-web-live --acl public-read
upload: StaticWebsite/401.html to s3://soshulnetwork-web-live/401.html
upload: StaticWebsite/404.html to s3://soshulnetwork-web-live/404.html
upload: StaticWebsite/500.html to s3://soshulnetwork-web-live/500.html
upload: StaticWebsite/LICENSE to s3://soshulnetwork-web-live/LICENSE
upload: StaticWebsite/assets/logo/favicon/android-chrome-512x512.png to s3://soshulnetwork-web-live/assets/logo/favicon/android-chrome-512x512.png
upload: StaticWebsite/assets/demo/chart-pie-demo.js to s3://soshulnetwork-web-live/assets/demo/chart-pie-demo.js
upload: StaticWebsite/assets/demo/datatables-demo.js to s3://soshulnetwork-web-live/assets/demo/datatables-demo.js
upload: StaticWebsite/assets/demo/chart-area-demo.js to s3://soshulnetwork-web-live/assets/demo/chart-area-demo.js
upload: StaticWebsite/assets/logo/favicon/apple-touch-icon.png to s3://soshulnetwork-web-live/assets/logo/favicon/apple-touch-icon.png
upload: StaticWebsite/assets/demo/chart-bar-demo.js to s3://soshulnetwork-web-live/assets/demo/chart-bar-demo.js
upload: StaticWebsite/assets/img/error-404-monochrome.svg to s3://soshulnetwork-web-live/assets/img/error-404-monochrome.svg
upload: StaticWebsite/assets/logo/favicon/favicon-32x32.png to s3://soshulnetwork-web-live/assets/logo/favicon/favicon-32x32.png
upload: StaticWebsite/assets/logo/favicon/android-chrome-192x192.png to s3://soshulnetwork-web-live/assets/logo/favicon/android-chrome-192x192.png
upload: StaticWebsite/assets/logo/favicon/favicon-16x16.png to s3://soshulnetwork-web-live/assets/logo/favicon/favicon-16x16.png
upload: StaticWebsite/assets/logo/favicon/favicon.ico to s3://soshulnetwork-web-live/assets/logo/favicon/favicon.ico
upload: StaticWebsite/assets/logo/full_black.png to s3://soshulnetwork-web-live/assets/logo/full_black.png
upload: StaticWebsite/assets/logo/full_logo_wht.png to s3://soshulnetwork-web-live/assets/logo/full_logo_wht.png
upload: StaticWebsite/assets/logo/full_logo_blk.png to s3://soshulnetwork-web-live/assets/logo/full_logo_blk.png
upload: StaticWebsite/assets/logo/logo_rounded.png to s3://soshulnetwork-web-live/assets/logo/logo_rounded.png
upload: StaticWebsite/beacon.html to s3://soshulnetwork-web-live/beacon.html
upload: StaticWebsite/assets/logo/full_black.xcf to s3://soshulnetwork-web-live/assets/logo/full_black.xcf
upload: StaticWebsite/assets/logo/full_white.xcf to s3://soshulnetwork-web-live/assets/logo/full_white.xcf
upload: StaticWebsite/assets/logo/logo.png to s3://soshulnetwork-web-live/assets/logo/logo.png
upload: StaticWebsite/assets/logo/logo.pptx to s3://soshulnetwork-web-live/assets/logo/logo.pptx
upload: StaticWebsite/cognito_login.html to s3://soshulnetwork-web-live/cognito_login.html
upload: StaticWebsite/charts.html to s3://soshulnetwork-web-live/charts.html
upload: StaticWebsite/cognito_logout.html to s3://soshulnetwork-web-live/cognito_logout.html
upload: StaticWebsite/js/beacon.js to s3://soshulnetwork-web-live/js/beacon.js
upload: StaticWebsite/index.html to s3://soshulnetwork-web-live/index.html
upload: StaticWebsite/css/toastr.min.css to s3://soshulnetwork-web-live/css/toastr.min.css
upload: StaticWebsite/favicon.ico to s3://soshulnetwork-web-live/favicon.ico
upload: StaticWebsite/css/font-awesome.css to s3://soshulnetwork-web-live/css/font-awesome.css
upload: StaticWebsite/js/cognito_login.js to s3://soshulnetwork-web-live/js/cognito_login.js
upload: StaticWebsite/js/cognito_logout.js to s3://soshulnetwork-web-live/js/cognito_logout.js
upload: StaticWebsite/js/datatables-simple-demo.js to s3://soshulnetwork-web-live/js/datatables-simple-demo.js
upload: StaticWebsite/js/bootstrap.bundle.min.js to s3://soshulnetwork-web-live/js/bootstrap.bundle.min.js
upload: StaticWebsite/js/home.js to s3://soshulnetwork-web-live/js/home.js
upload: StaticWebsite/js/my_beacons.js to s3://soshulnetwork-web-live/js/my_beacons.js
upload: StaticWebsite/js/scripts.js to s3://soshulnetwork-web-live/js/scripts.js
upload: StaticWebsite/js/ssn.js to s3://soshulnetwork-web-live/js/ssn.js
upload: StaticWebsite/js/toastr.min.js to s3://soshulnetwork-web-live/js/toastr.min.js
upload: StaticWebsite/js/moment.min.js to s3://soshulnetwork-web-live/js/moment.min.js
upload: StaticWebsite/layout-sidenav-light.html to s3://soshulnetwork-web-live/layout-sidenav-light.html
upload: StaticWebsite/js/jquery-3.6.0.min.js to s3://soshulnetwork-web-live/js/jquery-3.6.0.min.js
upload: StaticWebsite/login.html to s3://soshulnetwork-web-live/login.html
upload: StaticWebsite/layout-static.html to s3://soshulnetwork-web-live/layout-static.html
upload: StaticWebsite/css/styles.css to s3://soshulnetwork-web-live/css/styles.css
upload: StaticWebsite/my_beacons.html to s3://soshulnetwork-web-live/my_beacons.html
upload: StaticWebsite/password.html to s3://soshulnetwork-web-live/password.html
upload: StaticWebsite/register.html to s3://soshulnetwork-web-live/register.html
upload: StaticWebsite/t_and_c.html to s3://soshulnetwork-web-live/t_and_c.html
upload: StaticWebsite/test.html to s3://soshulnetwork-web-live/test.html
upload: StaticWebsite/tables.html to s3://soshulnetwork-web-live/tables.html
upload: StaticWebsite/js/fontawesome.min.js to s3://soshulnetwork-web-live/js/fontawesome.min.js
```

**Enable Static Website Hosting**
```
$ ./s3_enable_website.sh
> aws s3 website s3://soshulnetwork-web-live --index-document index.html --error-document 404.html
> Test static webpage:
It's working.
> The static website URL:
http://soshulnetwork-web-live.s3-website.us-west-1.amazonaws.com/
```