#!/bin/sh
# Empty the bucket
echo "> aws s3 rm s3://soshulnetwork-web-live --recursive"
aws s3 rm s3://soshulnetwork-web-live --recursive
# Remove the bucket
echo "> aws s3 rb s3://soshulnetwork-web-live"
aws s3 rb s3://soshulnetwork-web-live

