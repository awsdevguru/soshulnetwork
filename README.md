# AWS Web Application Development - Pearson Online Learning

## Soshul.Network Source Code

This repository contains code and scripts to deploy the Soshul.Network social network website created as part of the online course, "AWS Web Application Development" provided by Pearson Online Learning.

## AWS CLI
Many scripts in this repository use the AWS command-line interface (CLI).  You must have AWS CLI installed and configured with programmatic user credentials.
```
$ aws configure
AWS Access Key ID [None]: <INSERT_ACCESS_KEY>
AWS Secret Access Key [None]: <INSERT_SECRET_KEY>
Default region name [None]: us-west-1
Default output format [None]: json
$
```

# Install Guide
The file INSTALL.md contains all of the steps to deploy this in your account.



# NOTE for 20220325 Students
The reason the live demo failed was due to the Cognito User Pool existing in the us-east-1 region while the Lambda functions were all instantiating the AWS object to region us-west-1.  Unfortunately this was not quickly resolved because of the way the code is structured.