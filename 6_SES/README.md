# Simple Email Service

**Example CLI Output**
```
$ ./ses_verify_identity.sh
Usage: ./ses_verify_identity.sh <email_address>

$ ./ses_verify_identity.sh nick@awsdev.guru
aws ses verify-email-identity --email-address nick@awsdev.guru
Command sent, click the link in the received email.
```

