#!/bin/sh

if [ $# -ne 1 ]; then
  echo "Usage: $0 <email_address>"
  exit
fi

email=$1
echo "aws ses verify-email-identity --email-address $email"
aws ses verify-email-identity --email-address $email
echo "Command sent, click the link in the received email."


echo "If you'd like to verify the domain via CLI, read:"
echo "aws ses verify-domain-identity help"
echo "You'll have to add a TXT record to the zone manually."
