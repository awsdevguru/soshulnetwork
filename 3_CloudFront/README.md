# Cloudfront

A Cloudfront distribution is used to serve the apex URL live.soshul.network with a valid ssl certificate.  

The distribution is created using the S3 bucket as the source.  ACM is used to garner a valid certificate for the top-level domain.
