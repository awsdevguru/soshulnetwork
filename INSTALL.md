The following script is used when building the Soshul.network platform.

**NOTE:**  
All of the scripts and code in this repository assume everything is being done in the us-west-1 region (Northern California). If you deploy services outside of us-west-1 changes will need to be made to the code, particularly the Lambda functions.

# Repo Clone
Clone the SoshulNetwork Repository locally
```
git clone https://tsnick@bitbucket.org/awsdevguru/soshulnetwork.git
```

# Identity and Access Management (IAM)
Create a new user called SSN, create programmatic access credentials.

# Command-line Interface (CLI)
Install AWS CLI, assuming Python and PIP are already installed.  

```
pip install awscli
```  

Configure the AWS CLI to use the new programmatic user created in the previous step.
```
nick@xps:~$ aws configure
AWS Access Key ID [****************J26E]: ABCDEFGH123456
AWS Secret Access Key [****************SAxS]: SuperSecretRandomKey
Default region name [us-west-1]: 
Default output format [json]: 
```


# IAM & Cognito
## Create Lambda Execution Role
Create Policy for use with Role that will be associated with all Lambda Functions.  
```
nick@xps:~/ssn/soshulnetwork/1_IAM$ ./create_lambda_policy.sh 
{
    "Policy": {
        "PolicyName": "SSN_Lambda_Policy_Live",
        "PolicyId": "ANPASEMQS3VFZLC5JMTO2",
        "Arn": "arn:aws:iam::146868985163:policy/SSN_Lambda_Policy_Live",
        "Path": "/",
        "DefaultVersionId": "v1",
        "AttachmentCount": 0,
        "PermissionsBoundaryUsageCount": 0,
        "IsAttachable": true,
        "CreateDate": "2022-03-28T16:33:55Z",
        "UpdateDate": "2022-03-28T16:33:55Z"
    }
}
nick@xps:~/ssn/soshulnetwork/1_IAM$ 
```

Create Execution Role referencing the policy that was just created.  
```
nick@xps:~/ssn/soshulnetwork/1_IAM$ ./create_lambda_execution_role.sh 
{
    "Role": {
        "Path": "/",
        "RoleName": "SSN_Lambda_Execution_Role_Live",
        "RoleId": "AROASEMQS3VF3CGGS7WBM",
        "Arn": "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live",
        "CreateDate": "2022-03-28T16:37:21Z",
        "AssumeRolePolicyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "lambda.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole"
                }
            ]
        }
    }
}
nick@xps:~/ssn/soshulnetwork/1_IAM$ 
```

## Create Cognito User Pool  
**_Ensure this is done in us-west-1_**

**Step 1: Sign-in experience**  
Provider Type: Cognito User Pool  
Sign-in options: Email only  
Next  

**Step 2: Security Reqs** 
Password Policy: Cognito defaults  
MFA: None  
User Account Recovery: Enable self-service, Email only  

**Step 3: Sign-up experience**  
**Disable** self-registration  
Attribute verfiication: Allow cognito to send messages and confirm  
Send email message, verify email address  
Required Attirbutes: Name only  
Custom Atributes: None  

**Step 4: Configure Message Delivery**  
Send email with Cognito, us-west-1  
From: default no-reply@verificationemail.com  

**Step 5: Integrate your app**  
User pool name: SoshulNetworkUserPoolLive  
Hosted auth: Use the Cognito Hosted UI  
Domain: Use a cognito domain, https://soshulnetworklive  
Initial App Client: Public client, name: SoshulNetworkAppLive  
Don’t generate a client secret  
Callback url: https://live.soshul.network/cognito_login.html  
Advanced: Token Expiration should be one day  
Advanced: OAuth 2.0 Grant Types: Add **Implicit Grant**  
Add sign-out URL: [https://live.soshul.network/cognito_logout.html](https://live.soshul.network/cognito_logout.html)  

**Step 6: Review and create, click Create**

### Customize Hosted UI Logo
Go into User Pool → App Integration
Go into App Client at bottom of screen  
View Hosted UI, no logo  
Go back to User Pool → App Integration → App Client → Hosted UI Customization → Use Client Level Settings  
Edit Logo in Hosted UI Customization, _repo_/2_S3/StaticWebsite/assets/logo/full_logo_blk.png  

### Create a user
Go into User Pool → App Integration → Users  
Create a user
View Hosted UI in App Client, log in as the user you just created and change the password from the initial password and set a Name attribute.  
Upon submittal of the password change form, you will get _"This site can't be reached"_ This is because the callback URL /cognito_login.html is not up yet.  

### Update Cognito URL in ssn.js
Edit ssn.js in soshulnetwork/2_S3/StaticWebsite/js  
Modify INSERT_COGNITO_LOGIN_URL to reference the full URL to the Cognito Hosted UI.
NOTE: Change the word "code" to "token" in the URL.

# Simple Storage Service (S3)
Create the bucket that will store our static website content.  
**NOTE:** There's a good chance the bucket name you choose will not be available, you have to enter a globally unique bucket name.

## Create Bucket
```
nick@xps:~/ssn/soshulnetwork/2_S3$ ./s3_make_bucket_website.sh 
Please enter the name of the bucket: soshulnetworkweblive
> aws s3 mb s3://soshulnetworkweblive
make_bucket: soshulnetworkweblive
> aws s3api put-bucket-versioning --bucket soshulnetworkweblive --versioning-configuration '{ MFADelete: Disabled, Status: Enabled }'
nick@xps:~/ssn/soshulnetwork/2_S3$ 
```

## Copy Static Website Content  
```
nick@xps:~/ssn/soshulnetwork/2_S3$ ./s3_sync_website.sh 
Please enter the name of the bucket: soshulnetworkweblive
> aws s3 sync ./StaticWebsite s3://soshulnetworkweblive --acl public-read
upload: StaticWebsite/401.html to s3://soshulnetworkweblive/401.html
upload: StaticWebsite/assets/logo/favicon/android-chrome-192x192.png to s3://soshulnetworkweblive/assets/logo/favicon/android-chrome-192x192.png
upload: StaticWebsite/assets/demo/chart-bar-demo.js to s3://soshulnetworkweblive/assets/demo/chart-bar-demo.js
upload: StaticWebsite/assets/demo/chart-pie-demo.js to s3://soshulnetworkweblive/assets/demo/chart-pie-demo.js
upload: StaticWebsite/404.html to s3://soshulnetworkweblive/404.html
upload: StaticWebsite/500.html to s3://soshulnetworkweblive/500.html
upload: StaticWebsite/LICENSE to s3://soshulnetworkweblive/LICENSE
upload: StaticWebsite/assets/demo/datatables-demo.js to s3://soshulnetworkweblive/assets/demo/datatables-demo.js
upload: StaticWebsite/assets/img/error-404-monochrome.svg to s3://soshulnetworkweblive/assets/img/error-404-monochrome.svg
upload: StaticWebsite/assets/logo/favicon/android-chrome-512x512.png to s3://soshulnetworkweblive/assets/logo/favicon/android-chrome-512x512.png
upload: StaticWebsite/assets/demo/chart-area-demo.js to s3://soshulnetworkweblive/assets/demo/chart-area-demo.js
upload: StaticWebsite/assets/logo/favicon/apple-touch-icon.png to s3://soshulnetworkweblive/assets/logo/favicon/apple-touch-icon.png
upload: StaticWebsite/assets/logo/favicon/favicon-32x32.png to s3://soshulnetworkweblive/assets/logo/favicon/favicon-32x32.png
upload: StaticWebsite/assets/logo/full_black.png to s3://soshulnetworkweblive/assets/logo/full_black.png
upload: StaticWebsite/assets/logo/full_logo_blk.png to s3://soshulnetworkweblive/assets/logo/full_logo_blk.png
upload: StaticWebsite/assets/logo/favicon/favicon-16x16.png to s3://soshulnetworkweblive/assets/logo/favicon/favicon-16x16.png
upload: StaticWebsite/assets/logo/full_logo_wht.png to s3://soshulnetworkweblive/assets/logo/full_logo_wht.png
upload: StaticWebsite/assets/logo/favicon/favicon.ico to s3://soshulnetworkweblive/assets/logo/favicon/favicon.ico
upload: StaticWebsite/charts.html to s3://soshulnetworkweblive/charts.html
upload: StaticWebsite/assets/logo/logo.png to s3://soshulnetworkweblive/assets/logo/logo.png
upload: StaticWebsite/cognito_login.html to s3://soshulnetworkweblive/cognito_login.html
upload: StaticWebsite/assets/logo/full_white.xcf to s3://soshulnetworkweblive/assets/logo/full_white.xcf
upload: StaticWebsite/cognito_logout.html to s3://soshulnetworkweblive/cognito_logout.html
upload: StaticWebsite/assets/logo/full_black.xcf to s3://soshulnetworkweblive/assets/logo/full_black.xcf
upload: StaticWebsite/assets/logo/logo.pptx to s3://soshulnetworkweblive/assets/logo/logo.pptx
upload: StaticWebsite/assets/logo/logo_rounded.png to s3://soshulnetworkweblive/assets/logo/logo_rounded.png
upload: StaticWebsite/beacon.html to s3://soshulnetworkweblive/beacon.html
upload: StaticWebsite/css/font-awesome.css to s3://soshulnetworkweblive/css/font-awesome.css
upload: StaticWebsite/index.html to s3://soshulnetworkweblive/index.html
upload: StaticWebsite/css/toastr.min.css to s3://soshulnetworkweblive/css/toastr.min.css
upload: StaticWebsite/js/beacon.js to s3://soshulnetworkweblive/js/beacon.js
upload: StaticWebsite/favicon.ico to s3://soshulnetworkweblive/favicon.ico
upload: StaticWebsite/js/cognito_login.js to s3://soshulnetworkweblive/js/cognito_login.js
upload: StaticWebsite/js/cognito_logout.js to s3://soshulnetworkweblive/js/cognito_logout.js
upload: StaticWebsite/js/datatables-simple-demo.js to s3://soshulnetworkweblive/js/datatables-simple-demo.js
upload: StaticWebsite/js/bootstrap.bundle.min.js to s3://soshulnetworkweblive/js/bootstrap.bundle.min.js
upload: StaticWebsite/js/home.js to s3://soshulnetworkweblive/js/home.js
upload: StaticWebsite/js/my_beacons.js to s3://soshulnetworkweblive/js/my_beacons.js
upload: StaticWebsite/js/scripts.js to s3://soshulnetworkweblive/js/scripts.js
upload: StaticWebsite/js/toastr.min.js to s3://soshulnetworkweblive/js/toastr.min.js
upload: StaticWebsite/js/ssn.js to s3://soshulnetworkweblive/js/ssn.js
upload: StaticWebsite/js/moment.min.js to s3://soshulnetworkweblive/js/moment.min.js
upload: StaticWebsite/layout-static.html to s3://soshulnetworkweblive/layout-static.html
upload: StaticWebsite/login.html to s3://soshulnetworkweblive/login.html
upload: StaticWebsite/layout-sidenav-light.html to s3://soshulnetworkweblive/layout-sidenav-light.html
upload: StaticWebsite/css/styles.css to s3://soshulnetworkweblive/css/styles.css
upload: StaticWebsite/register.html to s3://soshulnetworkweblive/register.html
upload: StaticWebsite/password.html to s3://soshulnetworkweblive/password.html
upload: StaticWebsite/my_beacons.html to s3://soshulnetworkweblive/my_beacons.html
upload: StaticWebsite/js/jquery-3.6.0.min.js to s3://soshulnetworkweblive/js/jquery-3.6.0.min.js
upload: StaticWebsite/test.html to s3://soshulnetworkweblive/test.html
upload: StaticWebsite/t_and_c.html to s3://soshulnetworkweblive/t_and_c.html
upload: StaticWebsite/tables.html to s3://soshulnetworkweblive/tables.html
upload: StaticWebsite/js/fontawesome.min.js to s3://soshulnetworkweblive/js/fontawesome.min.js
nick@xps:~/ssn/soshulnetwork/2_S3$ 
```

## Set Bucket Policy
```
nick@xps:~/ssn/soshulnetwork/2_S3$ ./s3_enable_website.sh 
Please enter the name of the bucket: soshulnetworkweblive
> aws s3 website s3://soshulnetworkweblive --index-document index.html --error-document 404.html
Setting bucket policy
> Test static webpage:
It's working.
> The static website URL:
http://soshulnetworkweblive.s3-website.us-west-1.amazonaws.com/
nick@xps:~/ssn/soshulnetwork/2_S3$ 
```


# Amazon Certificate Manager (ACM)
We need a TLS certificate for our domain, in the case of the course, it's live.soshul.network.  

Request a public cert
FQDN: live.soshul.network  
DNS Validation  
No Tags  
See pending validation, enter cert ID  
See pending validation  
Click Create records in Route53  
Yes, create  
Refresh until Issued  

# Cloudfront
Create Distribution  
**Origin:** 
Select S3 bucket created in the S3 section  
Set a pretty name for the origin.  
Don’t use OAI  
No custom Headers  
No Origin Shield  
  
**Default Cache Behaviour:**  
Leave all default

**Function Associations:**  
Leave Default  

**Settings:**  
Use all edge locations  
No WAF  
Alternate domain name: **live.soshul.network**  
Reference certificate you created in the ACM section.  
TLS and HTTP/2 default  
default root object: index.html  
Standard logging off  
v6 on  
Create  
Wait for Deployed  

# Route53
Go into hosted zone  
Create Alias record for live.soshul.network, reference the CloudFront distribution ID  
Make sure you put the custom domain name on the CF distribution  
Surf to live.soshul.network, login, see auth fail in js console  

# DynamoDB
Edit create_table.py, set Access Key, Secret Key and region.  
Create DynamoDB table with two Local Secondary Indexes
```
nick@xps:~/ssn/soshulnetwork/4_DynamoDB$ ./create_table.py 
> Current tables

> Creating table
    Created table ARN: arn:aws:dynamodb:us-west-1:146868985163:table/SoshulNetworkLive

> Waiting for table state: ACTIVE
    Check 0: CREATING
    Check 1: CREATING
    Check 2: CREATING
    Check 3: CREATING
    Check 4: CREATING
    Check 5: CREATING
    Check 6: CREATING
    Check 7: ACTIVE

> Current tables
    SoshulNetworkLive

nick@xps:~/ssn/soshulnetwork/4_DynamoDB$ 
```

# Simple Queue Service (SQS)
Create Standard Queue in SQS to be used by the Lambda function that notifies users of a new Beacon.  
```
nick@xps:~/ssn/soshulnetwork/5_SQS$ ./sqs_create_queue.sh 
aws sqs create-queue --queue-name soshul-network-live
{
    "QueueUrl": "https://us-west-1.queue.amazonaws.com/146868985163/soshul-network-live"
}
nick@xps:~/ssn/soshulnetwork/5_SQS$ 
```

# Simple Email Service (SES)
We're using SES in Sandbox.

Add verified email identities, whichever email accounts will receive emails from users.  
Add verified domain  
Domain: live.soshul.network  
Uncheck default configuration set  
Verify via DKIM  
Advanced DKIM Settings  
Easy DKIM via Route53  
RSA_2048_BIT  
Publish DNS to Route53: Enabled  
DKIM Signatures: Enabled  
No tags  
Wait for verified

# API Gateway
Create new REST Api  
Import from SSNL_APIG_Default_Swagger_All_Mock.json in soshulnetwork/7_API_Gateway  
Actions → Deploy, new stage named default  
Enable X-Ray tracing after deployment  

## Create Cognito Authorizer
Authorizers  
Create New Authorizer  
Name: SSNL_Cognito  
Type: Cognito  
Select user pool in the list for us-west-1  
Token Source: Authorization
Create

## Attach Authorizer to Resources/Methods
In API → Resources
Click /authenticated → GET method  
Click Method Request
Click Pencil icon next to Authorization
Select SSNL_Cognito authorizer, might need to refresh the page for the authorizer to show up.
Click Checkmark to set authorizer
Repeat this process for every method that is not OPTIONS.  OPTIONS should not be authorized.

## Update API Gateway URL
Edit ssn.js in soshulnetwork/2_S3/StaticWebsite/js  
Modify INSERT_API_GATEWAY_URL to reference the full URL to the API Gateway.
Run s3_sync_website in the 2_S3 directory to update ssn.js in the bucket.
```
nick@xps:~/ssn/soshulnetwork/2_S3$ ./s3_sync_website.sh 
Please enter the name of the bucket: soshulnetworkweblive
> aws s3 sync ./StaticWebsite s3://soshulnetworkweblive --acl public-read
upload: StaticWebsite/js/ssn.js to s3://soshulnetworkweblive/js/ssn.js
nick@xps:~/ssn/soshulnetwork/2_S3$ 
```

## Invalidate Cloudfront Cache
In Cloudfront distribution, invalidations tab
Create an invalidation for object path "/*"

# Lambda
Execute edit_files_ref_user_pool.sh in soshulnetwork/8_Lambda/Functions, replace all instances of INSERT_USER_POOL_ID with the Cognito user pool ID (which should be in us-west-1)
```
nick@xps:~/ssn/soshulnetwork/8_Lambda/Functions$ ./edit_files_ref_user_pool.sh 
Editing all files that reference the Cognito User Pool ID
6 files to edit
```

Edit SSN_POST_Beacon/SSN_POST_Beacon.js to reference the correct SQS Queue URL and region.
```
nick@xps:~/ssn/soshulnetwork/8_Lambda/Functions$ aws sqs list-queues
{
    "QueueUrls": [
        "https://us-west-1.queue.amazonaws.com/146868985163/soshul-network-live"
    ]
}
nick@xps:~/ssn/soshulnetwork/8_Lambda/Functions$ 
nick@xps:~/ssn/soshulnetwork/8_Lambda/Functions$ 
nick@xps:~/ssn/soshulnetwork/8_Lambda/Functions$ vi SSN_POST_Beacon/SSN_POST_Beacon.js
```


Execute deploy_all.sh to deploy all Lambda functions to Lambda
```
nick@xps:~/ssn/soshulnetwork/8_Lambda/Functions$ ./deploy_all.sh 
Deploying SSN_ANY_Not_Implemented
Packaging
rm: cannot remove '*.zip': No such file or directory
  adding: SSN_ANY_Not_Implemented.js (deflated 40%)
Create Function
{
    "FunctionName": "SSN_ANY_Not_Implemented",
    "FunctionArn": "arn:aws:lambda:us-west-1:146868985163:function:SSN_ANY_Not_Implemented",
    "Runtime": "nodejs14.x",
    "Role": "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live",
    "Handler": "SSN_ANY_Not_Implemented.handler",
    "CodeSize": 740,
    "Description": "",
    "Timeout": 3,
    "MemorySize": 128,
    "LastModified": "2022-03-28T18:04:39.864+0000",
    "CodeSha256": "eitPbngv2V6eGo4zhZMAVQDVKp8s4YYGM3aMvtR1O9g=",
    "Version": "$LATEST",
    "TracingConfig": {
        "Mode": "Active"
    },
    "RevisionId": "f59a5d72-c536-4ddf-a0b3-451144a75dc5",
    "State": "Pending",
    "StateReason": "The function is being created.",
    "StateReasonCode": "Creating",
    "PackageType": "Zip"
}
<output cut>
```

In Lambda Layers in Web Console, upload the x-ray layer zip file, soshulnetwork/8_Lambda/Layers/xray/xray.zip.
Associate the Layer with the Lambda Function: SSN_GET_Beacons

# SQS
Create a Lambda trigger on the SQS queue to trigger Lambda function SSN_Beacon_From_SQS_to_followers.

# API Gateway - Lambda Function Association
For each method (not OPTIONS), configure a Lambda Proxy Integration that points to the relevant Lambda function, verify cognito authorizer on each method  
for authenticated, go look at the lambda function and see the trigger attached  
PUT Beacon/UUID should be not implemented, leave as Mock  
POST and DELETE Following use the same function, SSN_POST_DELETE_Following   
Deploy API

# Test