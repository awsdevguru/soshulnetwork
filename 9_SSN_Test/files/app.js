var express = require('express');
var bodyParser = require('body-parser'); 
var request = require('request');
var port = process.env.PORT || 3000;
var app = express(),
path = require('path'),
publicDir = path.join(__dirname,'public');

app.use(express.static(publicDir));
app.use(bodyParser.json())

app.get('/hw', function (req, res) {
  res.send('hello world')
});

app.post("/test", function(req, res) {
    var options = {
      uri: req.body.path,
      method: req.body.verb,
      json: {},
      headers: {"Authorization":req.body.id_token, "Content-Type":"application/json"}
    };
    
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body) // Print the shortened url.
        res.send(JSON.stringify(body));
      } else {
        console.log(error);
        res.send(JSON.stringify(error));
      }
    });
});

app.listen(port);
module.exports = app;

