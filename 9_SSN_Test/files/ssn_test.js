$( document ).ready(function() {

  setInterval(updateClock, 1000);

  $("#btn-run").click(function(event) {
    event.preventDefault();

    var id_token = $("#id_token").val();
    var apig_url = $("#apig_url").val();
    
    if (id_token.length <= 0) {
        toastr.error("I need a token.");
    } else if (apig_url.length <= 0) {
        toastr.error("I need an API Gateway URL.");
    } else {
        run_tests(apig_url, id_token);
    }
  });
});

function run_tests(apig_url, id_token) {
  $("#results").empty();
  console.log("run_tests():: apig_url: " + apig_url, + " id_token: " + id_token);
  send_test("POST", apig_url, id_token, "/beacon/fake_uuid/dislike");
  send_test("DELETE", apig_url, id_token, "/beacon/fake_uuid");
  send_test("DELETE", apig_url, id_token, "/beacons");
  send_test("GET", apig_url, id_token, "/authenticated");
  send_test("GET", apig_url, id_token, "/beacon/fake_uuid");
  send_test("GET", apig_url, id_token, "/beacons");
  send_test("GET", apig_url, id_token, "/beacons/mine");
  send_test("GET", apig_url, id_token, "/beacon/fake_uuid/dislike");
  send_test("GET", apig_url, id_token, "/beacon/fake_uuid/like");
  send_test("GET", apig_url, id_token, "/user");
  send_test("GET", apig_url, id_token, "/user/fake_sub");
  send_test("POST", apig_url, id_token, "/beacon");
  send_test("POST", apig_url, id_token, "/user/fake_sub/following");
  send_test("DELETE", apig_url, id_token, "/user/fake_sub/following");
  send_test("POST", apig_url, id_token, "/beacon/fake_uuid/dislike");
  send_test("POST", apig_url, id_token, "/beacon/fake_uuid/like");
  send_test("POST", apig_url, id_token, "/beacons/search");
}

function send_test(verb, apig_url, id_token, path) {
  var url = "/test";
  var data = {
    path: apig_url + path,
    id_token: id_token,
    verb: verb
  }
  console.log("post data: " + JSON.stringify(data));
  
  $.ajax({
    type: "POST",
    url: url,
    data: JSON.stringify(data),
    dataType: 'json',
    contentType: 'application/json',
    success: function( data, textStatus, jQxhr ){
      $("#results").append('<li class="list-group-item"><b>Verb: </b>' + verb + '<br><b>Path:</b> ' + path + '<br><b>Results:</b> ' + JSON.stringify(data) + '</li>');
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
        toastr.error(errorThrown);
    }
  });
}

function updateClock() {
  var ts_d = new Date();
  //$("#clock").text(ts_d.toISOString());
  $("#clock").text(moment().format("YYYYMMDD -  HH:mm:ss z"));
}
