# IAM

## Create Lambda Policy for Execution Role

### CLI Command
```
#!/bin/sh

aws iam create-policy --policy-name "SSN_Lambda_Policy_Live" --policy-document '{ "Version": "2012-10-17", "Statement": [ { "Sid": "VisualEditor0", "Effect": "Allow", "Action": [ "dynamodb:BatchGetItem", "dynamodb:PutItem", "dynamodb:DescribeTable", "dynamodb:DeleteItem", "dynamodb:GetItem", "dynamodb:Scan", "dynamodb:Query", "dynamodb:UpdateItem" ], "Resource": [ "arn:aws:dynamodb:us-west-1:146868985163:table/SoshulNetworkLive", "arn:aws:dynamodb:us-west-1:146868985163:table/SoshulNetworkLive/index/*" ] }, { "Sid": "VisualEditor1", "Effect": "Allow", "Action": [ "sqs:DeleteMessage", "ses:SendEmail", "logs:CreateLogStream", "sqs:GetQueueUrl", "xray:PutTelemetryRecords", "sqs:ReceiveMessage", "sqs:SendMessage", "sqs:GetQueueAttributes", "logs:CreateLogGroup", "logs:PutLogEvents", "cognito-idp:AdminGetUser", "xray:PutTraceSegments" ], "Resource": "*" } ] }'
```

### Output
```
$ ./create_lambda_policy.sh
{
    "Policy": {
        "PolicyName": "SSN_Lambda_Policy_Live",
        "PolicyId": "ANPASEMQS3VF3S3UD3GZO",
        "Arn": "arn:aws:iam::146868985163:policy/SSN_Lambda_Policy_Live",
        "Path": "/",
        "DefaultVersionId": "v1",
        "AttachmentCount": 0,
        "PermissionsBoundaryUsageCount": 0,
        "IsAttachable": true,
        "CreateDate": "2022-01-19T01:59:05Z",
        "UpdateDate": "2022-01-19T01:59:05Z"
    }
}
```


## Create Lambda Execution Role
Uses previously created policy
### CLI Command
```
#!/bin/sh
#
# NOTE: Replace acct with your AWS account #
#

acct="146868985163"

#Create Role
aws iam create-role --role-name "SSN_Lambda_Execution_Role_Live" --assume-role-policy-document '{ "Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Principal": { "Service": "lambda.amazonaws.com" }, "Action": "sts:AssumeRole" } ] }' --description "Allow Lambda to call AWS services for SoshulNetwork"

# Attach Policy to new role
aws iam attach-role-policy --role-name "SSN_Lambda_Execution_Role_Live" --policy-arn "arn:aws:iam::$acct:policy/SSN_Lambda_Policy_Live"
```
### Output
```
$ ./create_lambda_execution_role.sh
{
    "Role": {
        "Path": "/",
        "RoleName": "SSN_Lambda_Execution_Role_Live",
        "RoleId": "AROASEMQS3VF4FXNLCU66",
        "Arn": "arn:aws:iam::146868985163:role/SSN_Lambda_Execution_Role_Live",
        "CreateDate": "2022-01-19T01:59:50Z",
        "AssumeRolePolicyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "lambda.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole"
                }
            ]
        }
    }
}
```




