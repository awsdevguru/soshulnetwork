#!/bin/sh
#
# NOTE: Replace acct with your AWS account #
#

acct="146868985163"

#Create Role
aws iam create-role --role-name "SSN_Lambda_Execution_Role_Live" --assume-role-policy-document '{ "Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Principal": { "Service": "lambda.amazonaws.com" }, "Action": "sts:AssumeRole" } ] }' --description "Allow Lambda to call AWS services for SoshulNetwork"

# Attach Policy to new role
aws iam attach-role-policy --role-name "SSN_Lambda_Execution_Role_Live" --policy-arn "arn:aws:iam::$acct:policy/SSN_Lambda_Policy_Live"
