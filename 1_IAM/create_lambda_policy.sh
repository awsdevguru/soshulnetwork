#!/bin/sh
#
# NOTE: Replace acct (146...) with your AWS account #
#

aws iam create-policy --policy-name "SSN_Lambda_Policy_Live" --policy-document '{ "Version": "2012-10-17", "Statement": [ { "Sid": "VisualEditor0", "Effect": "Allow", "Action": [ "dynamodb:BatchGetItem", "dynamodb:PutItem", "dynamodb:DescribeTable", "dynamodb:DeleteItem", "dynamodb:GetItem", "dynamodb:Scan", "dynamodb:Query", "dynamodb:UpdateItem" ], "Resource": [ "arn:aws:dynamodb:us-west-1:146868985163:table/SoshulNetworkLive", "arn:aws:dynamodb:us-west-1:146868985163:table/SoshulNetworkLive/index/*" ] }, { "Sid": "VisualEditor1", "Effect": "Allow", "Action": [ "sqs:DeleteMessage", "ses:SendEmail", "logs:CreateLogStream", "sqs:GetQueueUrl", "xray:PutTelemetryRecords", "sqs:ReceiveMessage", "sqs:SendMessage", "sqs:GetQueueAttributes", "logs:CreateLogGroup", "logs:PutLogEvents", "cognito-idp:AdminGetUser", "xray:PutTraceSegments" ], "Resource": "*" } ] }'
