#!/bin/sh
API_URL="INSERT_API_GATEWAY_URL"
TOKEN="INSERT_ID_TOKEN"

k=0

while [ $k -lt 100 ]
do
  echo Sending request $k
  curl -s -H "Authorization: $TOKEN" $API_URL/beacons > /dev/null
  k=`expr $k + 1`
done
