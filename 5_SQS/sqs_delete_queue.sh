#!/bin/sh
echo "Getting Queue URL..."
echo "aws sqs list-queues --queue-name-prefix \"soshul-network-live\" | jq \".QueueUrls[0]\" | sed 's/\"//g'"
queue_url=`aws sqs list-queues --queue-name-prefix "soshul-network-live" | jq ".QueueUrls[0]" | sed 's/\"//g'`
echo "Received queue_url: $queue_url"

echo "Deleting SQS Queue..."
echo "aws sqs delete-queue --queue-url $queue_url"
aws sqs delete-queue --queue-url $queue_url

