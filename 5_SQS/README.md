# Simple Queue Service
Used to broker messages for the notify-followers process


**Create Queue**
```
$ ./sqs_create_queue.sh
aws sqs create-queue --queue-name soshul-network-live
{
      "QueueUrl": "https://us-west-1.queue.amazonaws.com/146868985163/soshul-network-live"
}
```

**Get Queue URL**
```
$ ./sqs_get_queue_url.sh
aws sqs list-queues --queue-name-prefix "soshul-network-live"
{
      "QueueUrls": [
              "https://us-west-1.queue.amazonaws.com/146868985163/soshul-network-live"
                  ]
}
```

**Delete Queue**
$ ./sqs_delete_queue.sh
Getting Queue URL...
aws sqs list-queues --queue-name-prefix "soshul-network-live" | jq ".QueueUrls[0]" | sed 's/"//g'
Received queue_url: https://us-west-1.queue.amazonaws.com/146868985163/soshul-network-live
Deleting SQS Queue...
aws sqs delete-queue --queue-url https://us-west-1.queue.amazonaws.com/146868985163/soshul-network-live
```
