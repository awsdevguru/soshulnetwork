#!/usr/bin/python3
import json
import time
import boto3    

tableName = "SoshulNetworkLive" 

client = boto3.client('dynamodb', aws_access_key_id ='INSERT_KEY_ID', aws_secret_access_key='INSERT_SECRET', region_name='INSERT_REGION')

# 
# List current tables
#
def list_tables():
    tables = client.list_tables()
    print("> Current tables")
    for table in tables["TableNames"]:
      print("    " + table)
    print("")

list_tables()

#
# Delete table
#
print("> Deleting table")
response = client.delete_table(TableName=tableName)
print(response["TableDescription"]["TableName"] + ": " + response["TableDescription"]["TableStatus"])

