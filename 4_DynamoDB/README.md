# Overview
Python scripts to create/delete the Soshul.Network DynamoDB table and insert sample data for testing..

# Install Reqs
```
pip3 install -r requirements.txt
```

# Create Soshul.Network DDB Table
```
$ python3 create_table.py
> Current tables

> Creating table
    Created table ARN: arn:aws:dynamodb:us-west-1:146868985163:table/SoshulNetworkLive

> Waiting for table state: ACTIVE
    Check 0: CREATING
    Check 1: CREATING
    Check 2: CREATING
    Check 3: CREATING
    Check 4: CREATING
    Check 5: CREATING
    Check 6: CREATING
    Check 7: ACTIVE

> Current tables
    SoshulNetworkLive


```

# Delete Table (if needed)
```
$ python3 delete_table.py
> Current tables
    SoshulNetworkLive

> Deleting table
SoshulNetworkLive: DELETING

```
