#!/usr/bin/python3
import json
import time
import boto3    

tableName = "SoshulNetworkLive" 

client = boto3.client('dynamodb', aws_access_key_id ='INSERT_KEY_ID', aws_secret_access_key='INSERT_SECRET', region_name='INSERT_REGION')

# 
# List current tables
#
def list_tables():
    tables = client.list_tables()
    print("> Current tables")
    for table in tables["TableNames"]:
      print("    " + table)
    print("")

list_tables()

#
# Create table
#
print("> Creating table")
response = client.create_table(
    AttributeDefinitions=[ { 'AttributeName': 'pk', 'AttributeType': 'S' }, 
                           { 'AttributeName': 'sk', 'AttributeType': 'S' },
                           { 'AttributeName': 'sub_uuid', 'AttributeType': 'S' },
                           { 'AttributeName': 'beacon_uuid', 'AttributeType': 'S' } ],
    TableName=tableName,
    KeySchema=[ { 'AttributeName': 'pk', 'KeyType': 'HASH' },
                { 'AttributeName': 'sk', 'KeyType': 'RANGE' } ],
    ProvisionedThroughput={ 'ReadCapacityUnits': 1, 'WriteCapacityUnits': 1 },
    SSESpecification={ 'Enabled': False },
    LocalSecondaryIndexes=[
        {
            'IndexName': 'beacon_uuid-index',
            'KeySchema': [
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'beacon_uuid',
                    'KeyType': 'RANGE'
                },
            ],
            'Projection': {
                'ProjectionType': 'INCLUDE',
                'NonKeyAttributes': [
                    'sub_uuid',
                ]
            }
        },
        {
            'IndexName': 'sub_uuid-index',
            'KeySchema': [
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'sub_uuid',
                    'KeyType': 'RANGE'
                },
            ],
            'Projection': {
                'ProjectionType': 'INCLUDE',
                'NonKeyAttributes': [
                    'beacon_uuid',
                ]
            }
        },
    ]
)
print("    Created table ARN: " + response["TableDescription"]["TableArn"] + "\n")


#
# Wait for table to be ACTIVE
#
print("> Waiting for table state: ACTIVE")
c = 0
while c < 15:
  table_state = client.describe_table(TableName=tableName)
  state = table_state["Table"]["TableStatus"]
  print("    Check " + str(c) + ": " + state)
  if (state == "ACTIVE"): 
    c = 15
  else:
    c += 1
    time.sleep(1)
  

# 
# List Tables
#
print()
list_tables()
